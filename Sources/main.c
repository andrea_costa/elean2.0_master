/*
 * Copyright (c) 2015 - 2016 , Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/* MODULE main */

/* Including needed modules to compile this module/procedure */
#include "eLean.h"

/* eLean config struct */
eLean_config_t    eLean_config;

/* Exit variable */
volatile int      exit_code = 0;

/* eLean core structure */
volatile core_t   core;

/* CAN buffer struct */
flexcan_msgbuf_t  flexcan0_msgbuf;

/* Debug variables */
char              TXbuf[128];
uint8_t           RTS = false;


int main(void)
{
  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  #ifdef PEX_RTOS_INIT
    PEX_RTOS_INIT();                   /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of Processor Expert internal initialization.                    ***/

  /* eLean configurations */
  eLean_config.TRM0_ticks         = 750;
  eLean_config.TRM1_ticks         = 3750;
  eLean_config.CAN_baudrate       = BAUDRATE_1000K;
  eLean_config.CAN_RxFIFO_enabled = false;
  eLean_config.iam20680_selftest  = true;
  eLean_config.lis3mdl_selftest   = true;

  /* Initialize HW and Pheriperals */
  SystemSetup(eLean_config);

  /* Initialize app variables */
  AppSetup(eLean_config);

  print("** eLean 2.0 **\n\n");

  /* Start timers */
  flexTimer_start(true, true);


//#define IAM20680_TEST_NRT_LOOP
//#define LIS3MDL_TEST_NRT_LOOP

#if (defined(IAM20680_TEST_NRT_LOOP) || defined(LIS3MDL_TEST_NRT_LOOP))
  iam20680_data_t  iam20680_data   = {0};
  uint16_t arrigon = 0;
#endif

  /* Watchdog feed counter - no RealTime loop */
  uint16_t noRT_cnt = 0;

  while(1)
  {
    /* Reset SBC Watchdog */
    noRT_cnt++;

    /* Reset watchdog timer every 100 ms */
    if (noRT_cnt > 100)
    {
      noRT_cnt = 0;
      SBC_FeedWatchdog();
    }

    /* Debug function */
    if (RTS)
    {
      print("Loop\n");

      /* Clear non RT debug flag */
      RTS = false;
    }


    /* Debug code */
#if (defined(IAM20680_TEST_NRT_LOOP) || defined(LIS3MDL_TEST_NRT_LOOP))
    arrigon++;

    if (arrigon > 1000)
    {
#ifdef LIS3MDL_TEST_NRT_LOOP
      /* Read IMU data */
      iam20680_ReadIMU(&iam20680_data);

      sprintf(TXbuf, "%d %d %d\n", iam20680_data.ax, iam20680_data.ay, iam20680_data.az);
      print(TXbuf);
#endif

#ifdef LIS3MDL_TEST_NRT_LOOP
      /* Get magnetometer status */
      lis3mdl_ReadStatus();

      /* If data is available... */
      if (lis3mdl_GetPtrStatus()->bits.ZYXDA == 1)
      {
        /* Read magnetometer data */
        lis3mdl_ReadData();

        sprintf(TXbuf, "Data: %d %d %d | %d (%.2f)\n",  lis3mdl_GetPtrData()->x,
                                                        lis3mdl_GetPtrData()->y,
                                                        lis3mdl_GetPtrData()->z,
                                                        lis3mdl_GetPtrData()->temperature,
                                                        ((double)lis3mdl_GetPtrData()->temperature/256.0) + 25.0);
        print(TXbuf);
      }
#endif
      arrigon = 0;
    }
#endif

    OSIF_TimeDelay(1);
  }


  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
** @}
*/


/*
** ###################################################################
**
**     This file was created by Processor Expert 10.1 [05.21]
**     for the Freescale S32K series of microcontrollers.
**
** ###################################################################
*/

