/*
 * iam20680.h
 *
 *  Created on: 12 oct 2018
 *      Author: Claudio Scandella
 */

#ifndef SOURCES_H_IAM20680_H_
#define SOURCES_H_IAM20680_H_

#include <stdint.h>

enum
{
  IAM20680_IDLE,
  IAM20680_CONFIGURED,
  IAM20680_ERROR
};

enum
{
  IAM20680_NO_ERROR,
  IAM20680_READ_ID_ERROR,
  IAM20680_SELFTEST_ERROR,
  IAM20680_GENERIC_ERROR,
  IAM20680_UNCONFIGURED,
};

#define IAM20680_ID_VAL                     (0xa9)
#define IAM20680_CLOCKING_SELECTION_001_VAL 0x01    // set internal oscillator (0x00) or auto-select between oscillator/MEMS clock source (0x01) (recommended)
#define IAM20680_RESET_VAL                  (0x80)  // Reset bit is enabled
#define IAM20680_SLEEP_MODE                 0       // Sleep mode is disabled. Only full power mode is supported.
#define IAM20680_NORMAL_MODE                0
#define IAM20680_SELFTEST_MODE              1
#define IAM20680_CLEAR_REGISTER             0
#define IAM20680_FIFO_RST                   0x04
#define IAM20680_FIFO_ENABLE                0x40
#define IAM20680_SIG_COND_RST               0x01
#define IAM20680_FIFO_EN                    0x40
#define IAM20680_FIFO_MODE                  0x40
#define IAM20680_NUM_ST_FIFO_READS          100
#define IAM20680_NUM_VAL_FOR_MEAN           10 // AC MOD 26/04/18
#define IAM20680_GYRO_X_ST                  0x80
#define IAM20680_GYRO_Y_ST                  0x40
#define IAM20680_GYRO_Z_ST                  0x20
#define IAM20680_ACCEL_X_ST                 0x80
#define IAM20680_ACCEL_Y_ST                 0x40
#define IAM20680_ACCEL_Z_ST                 0x20
#define IAM20680_DISABLE_ST                 0x1f

// self test threshold provvisorie
#define IAM20680_ACCEL_SELF_TEST_THRESHOLD  0
#define IAM20680_GYRO_SELF_TEST_THRESHOLD   0

#define IAM20680_WRITE_REGISTER_BITMASK     0b00000000
#define IAM20680_READ_REGISTER_BITMASK      0b10000000

typedef enum {
  IAM20680_SELF_TEST_X_GYRO = 0,
  IAM20680_SELF_TEST_Y_GYRO,
  IAM20680_SELF_TEST_Z_GYRO,
  IAM20680_RESERVED_3,
  IAM20680_RESERVED_4,
  IAM20680_RESERVED_5,
  IAM20680_RESERVED_6,
  IAM20680_RESERVED_7,
  IAM20680_RESERVED_8,
  IAM20680_RESERVED_9,
  IAM20680_RESERVED_10,
  IAM20680_RESERVED_11,
  IAM20680_RESERVED_12,
  IAM20680_SELF_TEST_X_ACCEL,
  IAM20680_SELF_TEST_Y_ACCEL,
  IAM20680_SELF_TEST_Z_ACCEL,
  IAM20680_RESERVED_16,
  IAM20680_RESERVED_17,
  IAM20680_RESERVED_18,
  IAM20680_XG_OFFS_USRH,
  IAM20680_XG_OFFS_USRL,
  IAM20680_YG_OFFS_USRH,
  IAM20680_YG_OFFS_USRL,
  IAM20680_ZG_OFFS_USRH,
  IAM20680_ZG_OFFS_USRL,
  IAM20680_SMPLRT_DIV,
  IAM20680_CONFIG,
  IAM20680_GYRO_CONFIG,
  IAM20680_ACCEL_CONFIG,
  IAM20680_ACCEL_CONFIG_2,
  IAM20680_LP_MODE_CFG,
  IAM20680_ACCEL_WOM_THR,
  IAM20680_RESERVED_32,
  IAM20680_RESERVED_33,
  IAM20680_RESERVED_34,
  FIFO_EN,
  IAM20680_RESERVED_36,
  IAM20680_RESERVED_37,
  IAM20680_RESERVED_38,
  IAM20680_RESERVED_39,
  IAM20680_RESERVED_40,
  IAM20680_RESERVED_41,
  IAM20680_RESERVED_42,
  IAM20680_RESERVED_43,
  IAM20680_RESERVED_44,
  IAM20680_RESERVED_45,
  IAM20680_RESERVED_46,
  IAM20680_RESERVED_47,
  IAM20680_RESERVED_48,
  IAM20680_RESERVED_49,
  IAM20680_RESERVED_50,
  IAM20680_RESERVED_51,
  IAM20680_RESERVED_52,
  IAM20680_RESERVED_53,
  IAM20680_FSYNC_INT,
  IAM20680_INT_PIN_CFG,
  IAM20680_INT_ENABLE,
  IAM20680_RESERVED_57,
  IAM20680_INT_STATUS,
  IAM20680_ACCEL_XOUT_H,
  IAM20680_ACCEL_XOUT_L,
  IAM20680_ACCEL_YOUT_H,
  IAM20680_ACCEL_YOUT_L,
  IAM20680_ACCEL_ZOUT_H,
  IAM20680_ACCEL_ZOUT_L,
  IAM20680_TEMP_OUT_H,
  IAM20680_TEMP_OUT_L,
  IAM20680_GYRO_XOUT_H,
  IAM20680_GYRO_XOUT_L,
  IAM20680_GYRO_YOUT_H,
  IAM20680_GYRO_YOUT_L,
  IAM20680_GYRO_ZOUT_H,
  IAM20680_GYRO_ZOUT_L,
  IAM20680_RESERVED_73,
  IAM20680_RESERVED_74,
  IAM20680_RESERVED_75,
  IAM20680_RESERVED_76,
  IAM20680_RESERVED_77,
  IAM20680_RESERVED_78,
  IAM20680_RESERVED_79,
  IAM20680_RESERVED_80,
  IAM20680_RESERVED_81,
  IAM20680_RESERVED_82,
  IAM20680_RESERVED_83,
  IAM20680_RESERVED_84,
  IAM20680_RESERVED_85,
  IAM20680_RESERVED_86,
  IAM20680_RESERVED_87,
  IAM20680_RESERVED_88,
  IAM20680_RESERVED_89,
  IAM20680_RESERVED_90,
  IAM20680_RESERVED_91,
  IAM20680_RESERVED_92,
  IAM20680_RESERVED_93,
  IAM20680_RESERVED_94,
  IAM20680_RESERVED_95,
  IAM20680_RESERVED_96,
  IAM20680_RESERVED_97,
  IAM20680_RESERVED_98,
  IAM20680_RESERVED_99,
  IAM20680_RESERVED_100,
  IAM20680_RESERVED_101,
  IAM20680_RESERVED_102,
  IAM20680_RESERVED_103,
  IAM20680_SIGNAL_PATH_RESET,
  IAM20680_ACCEL_INTEL_CTRL,
  IAM20680_USER_CTRL,
  IAM20680_PWR_MGMT_1,
  IAM20680_PWR_MGMT_2,
  IAM20680_RESERVED_109,
  IAM20680_RESERVED_110,
  IAM20680_RESERVED_111,
  IAM20680_RESERVED_112,
  IAM20680_RESERVED_113,
  IAM20680_FIFO_COUNTH,
  IAM20680_FIFO_COUNTL,
  IAM20680_FIFO_R_W,
  IAM20680_WHO_AM_I,
  IAM20680_RESERVED_118,
  IAM20680_XA_OFFSET_H,
  IAM20680_XA_OFFSET_L,
  IAM20680_RESERVED_121,
  IAM20680_YA_OFFSET_H,
  IAM20680_YA_OFFSET_L,
  IAM20680_RESERVED_124,
  IAM20680_ZA_OFFSET_H,
  IAM20680_ZA_OFFSET_L
} iam_20680_register_t;

typedef struct
{
    int16_t ax;
    int16_t ay;
    int16_t az;
    int16_t gx;
    int16_t gy;
    int16_t gz;
    int16_t temperature;
    int8_t  device_id;
} iam20680_data_t;



union CONFIG_register_t
{
    struct
    {
        unsigned char dlpf_cfg      : 3;
        unsigned char ext_sync_set  : 3;
        unsigned char fifo_mode     : 1;
        unsigned char reserved      : 1;
    } CONFIG_bits;
    unsigned char CONFIG_register;
} ;

union GYRO_CONFIG_register_t
{
    struct
    {
        unsigned char fchoice_b     : 2;
        unsigned char reserved      : 1;
        unsigned char fs_sel        : 2;
        unsigned char zg_st         : 1;
        unsigned char yg_st         : 1;
        unsigned char xg_st         : 1;
    } GYRO_CONFIG_bits;
    unsigned char GYRO_CONFIG_register;
} ;

union ACCEL_CONFIG_register_t
{
    struct
    {
        unsigned char reserved      : 3;
        unsigned char accel_fs_sel  : 2;
        unsigned char ya_st         : 1;
        unsigned char xa_st         : 1;
        unsigned char za_st         : 1;
    } ACCEL_CONFIG_bits;
    unsigned char ACCEL_CONFIG_register;
} ;

union ACCEL_CONFIG_2_register_t
{
    struct
    {
        unsigned char a_dlpf_cfg      : 3;
        unsigned char accel_fchoice_b : 1;
        unsigned char dec2_cfg        : 2;
        unsigned char reserved        : 2;
    } ACCEL_CONFIG_2_bits;
    unsigned char ACCEL_CONFIG_2_register;
} ;

union USER_CTRL_register_t
{
    struct
    {
        unsigned char sig_cond_rst  : 1;
        unsigned char reserved4     : 1;
        unsigned char fifo_rst      : 1;
        unsigned char reserved3     : 1;
        unsigned char i2c_if_dis    : 1;
        unsigned char reserved2     : 1;
        unsigned char fifo_en       : 1;
        unsigned char reserved1     : 1;
    } USER_CTRL_bits;
    unsigned char USER_CTRL_register;
} ;

union PWR_MGMT_1_register_t
{
    struct
    {
        unsigned char clksel        : 3;
        unsigned char temp_dis      : 1;
        unsigned char gyro_standby  : 1;
        unsigned char accel_cycle   : 1;
        unsigned char sleep         : 1;
        unsigned char device_reset  : 1;
    } PWR_MGMT_1_bits;
    unsigned char PWR_MGMT_1_register;
} ;

union PWR_MGMT_2_register_t
{
    struct
    {
        unsigned char stby_zg       : 1;
        unsigned char stby_yg       : 1;
        unsigned char stby_xg       : 1;
        unsigned char stby_za       : 1;
        unsigned char stby_ya       : 1;
        unsigned char stby_xa       : 1;
        unsigned char reserved      : 1;
        unsigned char fifo_lp_en    : 1;
    } PWR_MGMT_2_bits;
    unsigned char PWR_MGMT_2_register;
} ;

struct iam20680_mems_t
{
    uint8_t smplrt_div;
    union CONFIG_register_t config;
    union GYRO_CONFIG_register_t gyro_config;
    union ACCEL_CONFIG_register_t accel_config;
    union ACCEL_CONFIG_2_register_t accel_config_2;
    union USER_CTRL_register_t user_ctrl;
    union PWR_MGMT_1_register_t pwr_mgmt_1;
    union PWR_MGMT_2_register_t pwr_mgmt_2;
} ;

extern struct iam20680_mems_t iam20680_mems;

uint8_t iam20680_Configure (uint8_t self_test);

void iam20680_device_id(iam20680_data_t *data);

void iam20680_reset(uint8_t reset);

void iam20680_set_low_noise_mode(union PWR_MGMT_1_register_t pwr_mgmt_1, union PWR_MGMT_2_register_t pwr_mgmt_2);

void iam20680_set_SPI_I2C_mode(uint8_t i2c_disabled);

void iam20680_set_sample_rate(uint8_t divider);

void iam20680_set_gyro_bw_range (uint8_t gyro_dlpf_cfg, uint8_t gyro_fs_sel, uint8_t gyro_f_choice_b);

void iam20680_set_acc_bw_range(uint8_t accel_fs_sel, uint8_t accel_f_choice_b, uint8_t accel_dlpf_cfg);

void iam20680_ReadIMU(iam20680_data_t *data);

#endif /* SOURCES_H_IAM20680_H_ */
