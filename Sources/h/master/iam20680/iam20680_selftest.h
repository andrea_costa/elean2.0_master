/*
 * iam20680_selftest.h
 *
 *  Created on: 24 apr 2018
 *      Author: Andrea_Costa
 */

#ifndef SOURCES_H_IAM20680_SELFTEST_H_
#define SOURCES_H_IAM20680_SELFTEST_H_


uint8_t iam20680_self_test_procedure(volatile uint8_t * results);

void disable_self_test(uint8_t reg_address);
void enable_self_test_on_axis(unsigned int reg_address, unsigned int self_test_axis);
uint8_t check_self_test_outcome(int self_test_manufacturing_value, long int self_test_response);
void read_self_test_manufacturing_values(unsigned int * values);
uint16_t do_test(unsigned int reg_address, unsigned int self_test_axis, long int * normal_mode_mean_value_out, long int * self_test_mean_value_out);

#endif /* SOURCES_H_IAM20680_SELFTEST_H_ */
