/*
 * iam20680_configuration.h
 *
 *  Created on: 24 apr 2018
 *      Author: Andrea_Costa
 */

#ifndef SOURCES_H_IAM20680_CONFIGURATION_H_
#define SOURCES_H_IAM20680_CONFIGURATION_H_


/*
 * GYROSCOPE CONFIGURATION TABLE
 *
 * 3-db BW (Hz)  |   Noise BW (Hz)  |   Rate (kHz)  |   Temp sensor 3-db BW |   GYRO_CONFIGURATION
 * ----------------------------------------------------------------------------------------
 *      8173            8595.1              32                  4000        |           0
 *      3281            3451.0              32                  4000        |           1
 *       250             306.6               8                  4000        |           2
 *       176             177.0               1                   188        |           3
 *        92             108.6               1                    98        |           4
 *        41              59.0               1                    42        |           5
 *        20              30.5               1                    20        |           6
 *        10              15.6               1                    10        |           7
 *         5               8.0               1                     5        |           8
 *      3281            3451.0               8                  4000        |           9
 */
#define IAM20680_GYRO_CONFIGURATION                                                     7

/*
 * ACCELEROMETER CONFIGURATION TABLE
 *
 * 3-db BW (Hz)  |   Noise BW (Hz)  |   Rate (kHz)  |   ACCEL_CONFIGURATION
 * -------------------------------------------------+----------------------
 *    1046.0            1100.0               4      |            0
 *     218.1             235.0               1      |            1
 *      99.0             121.3               1      |            2
 *      44.8              61.5               1      |            3
 *      21.2              31.0               1      |            4
 *      10.2              15.5               1      |            5
 *       5.1               7.8               1      |            6
 *     420.0             441.6               1      |            7
 */
#define IAM20680_ACCEL_CONFIGURATION                             5

/*
 * Sample rate is MEMS output data rate (ODR).
 *
 * It is worth to GYROSCOPE only if 3 < GYRO_CONFIGURATION < 8
 * It is worth to ACCELEROMETER only if 1 < ACCEL_CONFIGURATION < 7
 *
 * If SAMPLE_RATE is not worth refer to Rate as specified in configuration table
 *
 * min value: 3.91 Hz   max value: 1000 Hz (default)
 */
#define IAM20680_SAMPLE_RATE             1000

/*
 * ACCELEROMETER range
 *
 * range (g) |  sensibility (LSB/g)  |   ACCEL_RANGE
 * ----------------------------------+--------------
 *    �2              16'384         |       2
 *    �4               8'192         |       4
 *    �8               4'096         |       8
 *   �16               2'048         |      16
 */
#define IAM20680_ACCEL_RANGE                16

/*
 * GYROSCOPE range
 *
 * range (dps) |  sensibility (LSB/dps)  |   GYRO_RANGE
 * --------------------------------------+-------------
 *    �250                131.0          |       250
 *    �500                 65.5          |       500
 *   �1000                 32.8          |      1000
 *   �2000                 16.4          |      2000
 */
#define IAM20680_GYRO_RANGE                      250

/*
 * Select between SPI or I2C communication mode (case sensitive)
 */
#define IAM20680_I2C_SPI_MODE   "SPI"
//#define IAM20680_I2C_MODE

/*
 * Turn on/off TEMPERATURE sensor (case sensitive)
 */
#define TEMP_SENS_ON_OFF    ON
//#define TEMP_SENS_OFF

/*
 * This disables/enables ACCELEROMETER/GYROSCOPE axis
 *
 * Setting to 1 means disable
 */
#define STBY_XA         0
#define STBY_YA         0
#define STBY_ZA         0
#define STBY_XG         0
#define STBY_YG         0
#define STBY_ZG         0


void iam20680_initialize_config_variables();

#endif /* SOURCES_H_IAM20680_CONFIGURATION_H_ */
