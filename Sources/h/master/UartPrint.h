/*
 * UartPrint.h
 *
 *  Created on: 15 giu 2018
 *      Author: Andrea_Costa
 */

#ifndef SOURCES_H_UARTPRINT_H_
#define SOURCES_H_UARTPRINT_H_

void print(char *str);

#endif /* SOURCES_H_UARTPRINT_H_ */
