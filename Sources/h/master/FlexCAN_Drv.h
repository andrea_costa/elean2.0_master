/*
 * FlexCanDrv.h
 *
 *  Created on: 17 mar 2018
 *      Author: Andrea Costa
 */

#ifndef SOURCES_FLEXCANDRV_H_
#define SOURCES_FLEXCANDRV_H_

#define FlexcanSwapBytesInWordIndex(index) (((index) & ~3U) + (3U - ((index) & 3U)))
#define FlexcanSwapBytesInWord(a, b) REV_BYTES_32(a, b)

#define FLEXCAN_BUFFER_SIZE 16

typedef enum
{
  BAUDRATE_500K,
  BAUDRATE_1000K
} can_baudrate_t;

/********************/
/* FLEX CAN MSGBUFF */
/********************/
typedef struct {
    uint32_t  cs;                         /* Code and Status */
    uint32_t  msgId;                      /* Message Buffer ID */
    uint8_t   data[64];                   /* Data bytes of the FlexCAN message */
    uint8_t   dataLen;                    /* Length of data in bytes */
    uint32_t  timestamp;                  /* Received message time */
} flexcan_msg_t;


typedef struct {
  uint8_t       frame_read_cnt;
  uint8_t       index;
  uint8_t       free_slot;
  uint8_t       buf_size;
  uint8_t       buf_full;
  flexcan_msg_t flexcan_msg[FLEXCAN_BUFFER_SIZE];
} flexcan_msgbuf_t;


/*! @brief FlexCAN message buffer CODE FOR Tx buffers*/
enum {
    FLEXCAN_TX_INACTIVE  = 0x08, /*!< MB is not active.*/
    FLEXCAN_TX_ABORT     = 0x09, /*!< MB is aborted.*/
    FLEXCAN_TX_DATA      = 0x0C, /*!< MB is a TX Data Frame(MB RTR must be 0).*/
    FLEXCAN_TX_REMOTE    = 0x1C, /*!< MB is a TX Remote Request Frame (MB RTR must be 1).*/
    FLEXCAN_TX_TANSWER   = 0x0E, /*!< MB is a TX Response Request Frame from.*/
                                 /*!  an incoming Remote Request Frame.*/
    FLEXCAN_TX_NOT_USED   = 0xF  /*!< Not used*/
};


  typedef enum {
        FLEXCAN_MSG_ID_STD,         /*!< Standard ID*/
        FLEXCAN_MSG_ID_EXT          /*!< Extended ID*/
  } flexcan_msgbuff_id_type_t;



  typedef struct {
      uint32_t code;                        /*!< MB code for TX or RX buffers.*/
                                            /*! Defined by flexcan_mb_code_rx_t and flexcan_mb_code_tx_t */
      flexcan_msgbuff_id_type_t msgIdType;  /*!< Type of message ID (standard or extended)*/
      uint32_t dataLen;                     /*!< Length of Data in Bytes*/
      bool fd_enable;
      uint8_t fd_padding;
      bool enable_brs;                   /* Enable bit rate switch*/
  } flexcan_msgbuff_code_status_t;


  /*! @brief ID formats for Rx FIFO
   * Implements : flexcan_rx_fifo_id_element_format_t_Class
   */
  typedef enum {
      FLEXCAN_RX_FIFO_ID_FORMAT_A, /*!< One full ID (standard and extended) per ID Filter Table element.*/
      FLEXCAN_RX_FIFO_ID_FORMAT_B, /*!< Two full standard IDs or two partial 14-bit (standard and
                                        extended) IDs per ID Filter Table element.*/
      FLEXCAN_RX_FIFO_ID_FORMAT_C, /*!< Four partial 8-bit Standard IDs per ID Filter Table element.*/
      FLEXCAN_RX_FIFO_ID_FORMAT_D  /*!< All frames rejected.*/
  } flexcan_rx_fifo_id_element_format_t;

  /*! @brief FlexCAN Rx FIFO ID filter table structure
   * Implements : flexcan_id_table_t_Class
   */
  typedef struct {
      bool isRemoteFrame;      /*!< Remote frame*/
      bool isExtendedFrame;    /*!< Extended frame*/
      uint32_t *idFilter;      /*!< Rx FIFO ID filter elements*/
  } flexcan_id_table_t;


  /* CAN FD extended data length DLC encoding */
  #define CAN_DLC_VALUE_12_BYTES                   9U
  #define CAN_DLC_VALUE_16_BYTES                   10U
  #define CAN_DLC_VALUE_20_BYTES                   11U
  #define CAN_DLC_VALUE_24_BYTES                   12U
  #define CAN_DLC_VALUE_32_BYTES                   13U
  #define CAN_DLC_VALUE_48_BYTES                   14U
  #define CAN_DLC_VALUE_64_BYTES                   15U

  #define CAN_ID_EXT_MASK                          0x3FFFFu
  #define CAN_ID_EXT_SHIFT                         0
  #define CAN_ID_EXT_WIDTH                         18

  #define CAN_ID_STD_MASK                          0x1FFC0000u
  #define CAN_ID_STD_SHIFT                         18
  #define CAN_ID_STD_WIDTH                         11

  #define CAN_ID_PRIO_MASK                         0xE0000000u
  #define CAN_ID_PRIO_SHIFT                        29
  #define CAN_ID_PRIO_WIDTH                        3
  /* CS Bit Fields */
  #define CAN_CS_TIME_STAMP_MASK                   0xFFFFu
  #define CAN_CS_TIME_STAMP_SHIFT                  0
  #define CAN_CS_TIME_STAMP_WIDTH                  16

  #define CAN_CS_DLC_MASK                          0xF0000u
  #define CAN_CS_DLC_SHIFT                         16
  #define CAN_CS_DLC_WIDTH                         4

  #define CAN_CS_RTR_MASK                          0x100000u
  #define CAN_CS_RTR_SHIFT                         20
  #define CAN_CS_RTR_WIDTH                         1

  #define CAN_CS_IDE_MASK                          0x200000u
  #define CAN_CS_IDE_SHIFT                         21
  #define CAN_CS_IDE_WIDTH                         1

  #define CAN_CS_SRR_MASK                          0x400000u
  #define CAN_CS_SRR_SHIFT                         22
  #define CAN_CS_SRR_WIDTH                         1

  #define CAN_CS_CODE_MASK                         0xF000000u
  #define CAN_CS_CODE_SHIFT                        24
  #define CAN_CS_CODE_WIDTH                        4

  #define CAN_MB_EDL_MASK                          0x80000000u
  #define CAN_MB_BRS_MASK                          0x40000000u


  status_t FlexCANInitRxFIFO(uint8_t istance, uint8_t MBused, can_baudrate_t baudrate);

  status_t FlexCANInit(uint8_t instance, uint8_t MBused, can_baudrate_t baudrate);

  void FLEXCAN_IRQHandler(uint8_t instance, flexcan_msgbuf_t *flexcan0_msgbuf);

  uint32_t FlexCANtransmit(uint8_t istance, uint32_t msgBuffIdx, uint8_t dataLen, uint16_t msgId, uint8_t *msgData);

  uint8_t FLEXCAN_ComputePayloadSize(uint8_t dlcValue);

  uint8_t CAN_ReadOneFramefromBufferQueue(flexcan_msgbuf_t *RxBuffer, flexcan_msg_t *RxFrame);

#endif /* SOURCES_FLEXCANDRV_H_ */
