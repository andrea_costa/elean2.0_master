/*
 * NVMData.h
 *
 *  Created on: 24 apr 2018
 *      Author: Andrea_Costa
 */

#ifndef SOURCES_H_NVMDATA_H_
#define SOURCES_H_NVMDATA_H_

/*
 * Struct containing data to save into the emulated EEPROM
 *
 * Add whatever desired field to the struct
 * In order to maintaint retrocompatibility, please be sure to ADD and not to
 * alter the order of the variables
 */
typedef union
{
  struct  NVData_EEE
  {
    uint8_t   Status;
    uint32_t  device_bootloader_fw_version_id;
    uint32_t  device_user_app_fw_version_id;
    uint32_t  device_last_reprog_date;
    uint32_t  reprogramming_tool_serial_no;
  } values;
  uint8_t rawBytes[sizeof(struct NVData_EEE)];
} NVData_EEE_t;


/*
 * Struct containing data to save into the emulated EEPROM
 *
 * Add whatever desired field to the struct
 * In order to maintaint retrocompatibility, please be sure to ADD and not to
 * alter the order of the variables
 */
typedef union
{
  struct  NVData_CalibrationSettings
  {
    uint32_t aa;
  } values;
  uint8_t rawBytes[sizeof(struct NVData_CalibrationSettings)];
} NVData_CalibrationSettings_t;


 typedef union
 {
    struct NVData_ECUSettings
    {
        uint32_t device_serial_no;
        uint32_t device_batch_no;
        uint32_t device_hw_version_id;
    } values;
    uint8_t rawBytes[sizeof(struct NVData_ECUSettings)]; //The size of this array must be the minimum such that sizeof(NVData_ECU_t) = sizeof(rawBytes)
} NVData_ECUSettings_t;



void MEMORY_Read_NVData_CalibrationSettings();
void MEMORY_Read_NVData_ECUSettings();

void MEMORY_EEE_readData();
uint32_t MEMORY_EEE_writeData();


NVData_EEE_t * MEMORY_Get_NVData_EEE();
NVData_CalibrationSettings_t * MEMORY_Get_NVData_CalibrationSettings();
NVData_ECUSettings_t * MEMORY_Get_NVData_ECUSettings();

#endif /* SOURCES_H_NVMDATA_H_ */
