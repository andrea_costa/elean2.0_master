/*
 * Interrupts.h
 *
 *  Created on: 26 apr 2018
 *      Author: Andrea_Costa
 */

#ifndef SOURCES_H_INTERRUPTS_H_
#define SOURCES_H_INTERRUPTS_H_

void ftmTimerMC0ISR (void);

void ftmTimerMC1ISR (void);

void CAN0_IRQHandler (void);

#endif /* SOURCES_H_INTERRUPTS_H_ */
