/*
 * HardwareConfig.h
 *
 *  Created on: 18/04/2018
 *      Author: Andrea Costa
 */

#ifndef SOURCES_HARDWARECONFIG_H_
#define SOURCES_HARDWARECONFIG_H_

  #define SCB_RST               9U
  #define SCB_RST_PORT          PTC

  #define IAM20680_INT          11U
  #define IAM20680_INT_PORT     PTE

  #define LIS3MDL_INT           9U
  #define LIS3MDL_INT_PORT      PTE

  #define LIS3MDL_DRDY          15U
  #define LIS3MDL_DRDY_PORT     PTD

  #define UJA1169_MODEL         0xC9

#endif /* SOURCES_HARDWARECONFIG_H_ */
