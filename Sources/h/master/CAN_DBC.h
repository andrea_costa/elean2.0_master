/*
 * CAN_DBC.h
 *
 *  Created on: 24 apr 2018
 *      Author: Andrea_Costa
 */

#ifndef SOURCES_H_CAN_DBC_H_
#define SOURCES_H_CAN_DBC_H_

  /* TX messages */
  #define Tx_CAN_MB_GYRO                  0x300
  #define Tx_CAN_MB_ACC                   0x301
  #define Tx_CAN_MB_POSITION              0x302
  #define Tx_CAN_MB_CONTROLLER            0x303
  #define Tx_CAN_MB_LOCALIZATION          0x304
  #define Tx_CAN_HUB_INTERFACE            0x354
  #define Tx_CAN_MB_TORQUE                0x356
  #define Tx_CAN_VCU_STATE                0x371
  #define Tx_CAN_TEST_MESSAGE             0x700
  #define Tx_CAN_TEST2_MESSAGE            0x701
  #define Tx_CAN_LIDAR_F_SETTING          0x740

  /* RX messages */
  #define Rx_CAN_HUB_L_DATA               0x704
  #define Rx_CAN_HUB_L_POS_CURR           0x450
  #define Rx_CAN_HUB_R_DATA               0x705
  #define Rx_CAN_HUB_R_POS_CURR           0x550
  #define Rx_CAN_LIDAR_FRONT_DETECTIONS   0x75D
  #define Rx_CAN_LIDAR_FRONT_DISTANCE     0x75E
  #define Rx_CAN_LIDAR_LEFT_DETECTIONS    0x754
  #define Rx_CAN_LIDAR_LEFT_DISTANCE      0x755
  #define Rx_CAN_LIDAR_REAR_DETECTIONS    0x757
  #define Rx_CAN_LIDAR_REAR_DISTANCE      0x758
  #define Rx_CAN_LIDAR_RIGHT_DETECTIONS   0x751
  #define Rx_CAN_LIDAR_RIGHT_DISTANCE     0x752
  #define Rx_CAN_NAV_U_REF                0x370

  #define Rx_CAN_RESET                    0x666

#endif /* SOURCES_H_CAN_DBC_H_ */
