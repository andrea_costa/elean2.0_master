/*
 * SystemSetup.h
 *
 *  Created on: 18/04/2018
 *      Author: Andrea Costa
 */

#ifndef SOURCES_HARDWARESETUP_H_
#define SOURCES_HARDWARESETUP_H_

void        SystemSetup(eLean_config_t eLean_config);
uint32_t    SBC_RecoverFromFNMC(void);
uint32_t    SBCSetup(void);
uint32_t    SBC_Configure(void);
uint32_t    NVMSetup(void);

void        enableFPU(void);
void        flexTimer_mc0(uint8_t priority, uint8_t startTimer, uint16_t ticks);
void        flexTimer_mc1(uint8_t priority, uint8_t startTimer, uint16_t ticks);
void        flexTimer_start(uint8_t TMR0, uint8_t TMR1);
void        flexTimer_stop(uint8_t TMR0, uint8_t TMR1);
void        AppSetup(eLean_config_t config);
uint8_t     IAM20680Setup(void);

#endif /* SOURCES_SYSTEMSETUP_H_ */
