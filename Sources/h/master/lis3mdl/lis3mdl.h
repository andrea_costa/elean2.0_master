#ifndef SOURCES_H_LIS3MDL_H_
#define SOURCES_H_LIS3MDL_H_


enum
{
  LIS3MDL_NO_ERROR,
  LIS3MDL_READ_ID_ERROR,
  LIS3MDL_SELFTEST_ERROR,
  LIS3MDL_GENERIC_ERROR,
  LIS3MDL_UNCONFIGURED,
};

#define LIS3MDL_READ_REGISTER_BITMASK							    0x80
#define LIS3MDL_READ_MULTI_REGISTER_BITMASK						0xC0
#define LIS3MDL_WRITE_REGISTER_BITMASK							  0x00

#define LIS3MDL_ID_VAL											          0x3D

/* Page id register definition */
#define LIS3MDL_WHO_AM_I                              0X0F
#define LIS3MDL_CTRL_REG1                             0X20
#define LIS3MDL_CTRL_REG2                             0X21
#define LIS3MDL_CTRL_REG3                             0X22
#define LIS3MDL_CTRL_REG4                             0X23
#define LIS3MDL_CTRL_REG5                             0X24
#define LIS3MDL_STATUS_REG                            0X27
#define LIS3MDL_OUT_X_L                               0X28
#define LIS3MDL_OUT_X_H                               0X29
#define LIS3MDL_OUT_Y_L                               0X2A
#define LIS3MDL_OUT_Y_H                               0X2B
#define LIS3MDL_OUT_Z_L                               0X2C
#define LIS3MDL_OUT_Z_H                               0X2D
#define LIS3MDL_TEMP_OUT_L                            0X2E
#define LIS3MDL_TEMP_OUT_H                            0X2F
#define LIS3MDL_INT_CFG                               0X30
#define LIS3MDL_INT_SRC                               0X31
#define LIS3MDL_INT_THS_L                             0X32
#define LIS3MDL_INT_THS_H                             0X33


/* SELF TEST VALUES */
#define MIN_ST_X                                      1711L
#define MAX_ST_X                                      5133L
#define MIN_ST_Y                                      1711L
#define MAX_ST_Y                                      5133L
#define MIN_ST_Z                                      171L
#define MAX_ST_Z                                      1711L


typedef struct
{
	int16_t 	x;
	int16_t 	y;
	int16_t 	z;
	int16_t		temperature;
	uint8_t 	device_id;
} lis3mdl_data_t;


union lis3mdl_ctrl_reg1_t
{
    struct
    {
        uint8_t ST 			  : 1;
        uint8_t FAST_ODR	: 1;
        uint8_t DO 			  : 3;
        uint8_t OM 			  : 2;
        uint8_t TEMP_EN 	: 1;
    } bits;
    unsigned char value;
};


union lis3mdl_ctrl_reg2_t
{
    struct
    {
        uint8_t pad0		  : 1;
        uint8_t pad1		  : 1;
        uint8_t SOFT_RST	: 1;
        uint8_t REBOOT		: 1;
        uint8_t pad4		  : 1;
        uint8_t FS 			  : 2;
        uint8_t pad7 		  : 1;
    } bits;
    unsigned char value;
};


union lis3mdl_ctrl_reg3_t
{
    struct
    {
        uint8_t MD        : 2;
        uint8_t sim       : 1;
        uint8_t pad3      : 1;
        uint8_t pad4      : 1;
        uint8_t LP        : 1;
        uint8_t pad6      : 1;
        uint8_t pad7      : 1;
    } bits;
    unsigned char value;
};


union lis3mdl_ctrl_reg4_t
{
    struct
    {
        uint8_t pad0		  : 1;
        uint8_t BLE			  : 1;
        uint8_t OMZ			  : 2;
        uint8_t pad4 		  : 1;
        uint8_t pad5 		  : 1;
        uint8_t pad6 		  : 1;
        uint8_t pad7 		  : 1;
    } bits;
    unsigned char value;
};


union lis3mdl_ctrl_reg5_t
{
    struct
    {
        uint8_t pad0 		  : 1;
        uint8_t pad1 		  : 1;
        uint8_t pad2 		  : 1;
        uint8_t pad3 		  : 1;
        uint8_t pad4 		  : 1;
        uint8_t pad5 		  : 1;
        uint8_t BDU 		  : 1;
        uint8_t FAST_READ	: 1;
    } bits;
    unsigned char value;
};


union lis3mdl_status_t
{
    struct
    {
        uint8_t XDA       : 1;
        uint8_t YDA       : 1;
        uint8_t ZDA       : 1;
        uint8_t ZYXDA     : 1;
        uint8_t XOR       : 1;
        uint8_t YOR       : 1;
        uint8_t ZOR       : 1;
        uint8_t ZYXOR     : 1;
    } bits;
    unsigned char value;
};


union lis3mdl_int_cfg_t
{
    struct
    {
        uint8_t IEN       : 1;
        uint8_t LIR       : 1;
        uint8_t IEA       : 1;
        uint8_t force3    : 1;
        uint8_t pad4      : 1;
        uint8_t ZIEN      : 1;
        uint8_t YIEN      : 1;
        uint8_t XIEN      : 1;
    } bits;
    unsigned char value;
};


union lis3mdl_int_src_t
{
    struct
    {
        uint8_t INT       : 1;
        uint8_t MROI      : 1;
        uint8_t NTH_Z     : 1;
        uint8_t NTH_Y     : 1;
        uint8_t NTH_X     : 1;
        uint8_t PTH_Z     : 1;
        uint8_t PTH_Y     : 1;
        uint8_t PTH_X     : 1;
    } bits;
    unsigned char value;
};


typedef struct
{
	union lis3mdl_ctrl_reg1_t   reg1;
	union lis3mdl_ctrl_reg2_t   reg2;
	union lis3mdl_ctrl_reg3_t   reg3;
	union lis3mdl_ctrl_reg4_t   reg4;
	union lis3mdl_ctrl_reg5_t   reg5;
	union lis3mdl_status_t      status;
	union lis3mdl_int_cfg_t 	  int_cfg;
	union lis3mdl_int_src_t 	  int_src;
	lis3mdl_data_t              data;
} lis3mdl_t;




uint8_t LIS3MDL_Init (void);
uint8_t lis3mdl_GetReg (uint8_t reg);
uint8_t lis3mdl_SettReg (uint8_t reg, uint8_t value);
void lis3mdl_GetStatus (lis3mdl_t *lis3mdl);
void lis3mdl_GetData (lis3mdl_data_t *data);
void lis3mdl_GetDataAndTemp (lis3mdl_data_t *data);
uint8_t LIS3MDL_SelfTest (uint8_t selftest);
void lis3mdl_InitConfigVariables (void);
lis3mdl_data_t * lis3mdl_GetPtrData (void);
void lis3mdl_ReadData (void);
void lis3mdl_ReadStatus (void);
void LIS3MDL_Setup (void);
union lis3mdl_status_t * lis3mdl_GetPtrStatus (void);

#endif
