#ifndef __LIS3MDL_CONFIGURATION_H__
#define __LIS3MDL_CONFIGURATION_H__

#define LIS3MDL_INTERFACE                  2       // defined by physical wiring
#define LIS3MDL_PRIMARY_PRESCALER          0b01
#define LIS3MDL_SECONDARY_PRESCALER        0b111
#define LIS3MDL_MODE_16                    0


/*
    OM1     OM0     Operating mode for X and Y axes | LIS3MDL_XY_OPERATION_MODE_CONFIG
    ----------------------------------------------- |           
     0       0      Low-power mode                  |           0
     0       1      Medium-performance mode         |           1
     1       0      High-performance mode           |           2
     1       1      Ultra-high-performance mode     |           3
*/
#define LIS3MDL_XY_OPERATION_MODE_CONFIG                        3

/*
    OMZ1    OMZ0    Operating mode for Z axes       | LIS3MDL_Z_OPERATION_MODE_CONFIG
    ----------------------------------------------- |           
     0       0      Low-power mode                  |           0
     0       1      Medium-performance mode         |           1
     1       0      High-performance mode           |           2
     1       1      Ultra-high-performance mode     |           3
*/
#define LIS3MDL_Z_OPERATION_MODE_CONFIG                         3

/*
    DO2     DO1     DO0     ODR [Hz]                | LIS3MDL_ODR
    ----------------------------------------------- |
     0       0       0      0.625                   |           0
     0       0       1      1.25                    |           1
     0       1       0      2.5                     |           2
     0       1       1      5                       |           3
     1       0       0      10                      |           4
     1       0       1      20                      |           5
     1       1       0      40                      |           6
     1       1       1      80                      |           7
*/
#define LIS3MDL_ODR                                             7



/*
    FAST_ODR    Desc                                | LIS3MDL_ODR_FAST
    ----------------------------------------------- |
     0          Disabled                            |           0
     1          Enabled                             |           1
*/
#define LIS3MDL_ODR_FAST                                        1




/*
        FS1     FS0     Full-scale                  | LIS3MDL_FULL_SCALE
        ------------------------------------------- |           
         0       0      �4 gauss                    |           0
         0       1      �8 gauss                    |           1
         1       0      �12 gauss                   |           2
         1       1      �16 gauss                   |           3
*/
#define LIS3MDL_FULL_SCALE                                      0


/*
        TEMP_EN     Sensor                          | LIS3MDL_TEMPERATURE_SENSOR_ENABLED
        ------------------------------------------- |           
            0       Disabled                        |           0
            1       Enabled                         |           1
*/
#define LIS3MDL_TEMPERATURE_SENSOR_ENABLED                      1


/*
        MD1     MD0     Mode                        | LIS3MDL_OPERATION_MODE
        ------------------------------------------- |
         0       0       Continuous-conversion mode |           0
         0       1       Single-conversion mode     |           1
                         Single-conversion mode has |
                         to be used with sampling   |
                         frequency from 0.625 Hz    |
                         to 80Hz.                   |
         1       0       Power-down mode            |           2
         1       1       Power-down mode            |           3
*/
#define LIS3MDL_OPERATION_MODE                                  0


/*
        SIM         SPI                             | LIS3MDL_SPI_CONF
        ------------------------------------------- |           
         0          4-Wire                          |           0
         1          3-Wire                          |           1
*/
#define LIS3MDL_SPI_CONF                                        0


/*
        LP          Sensor                          | LIS3MDL_LOW_POWER
        ------------------------------------------- |           
         0          No low power                    |           0
         1          If this bit is ?1?,             |           1
                    DO[2:0] is set to 0.625 Hz      |           
*/
#define LIS3MDL_LOW_POWER                                       0


/*
        XIEN        Interrupt                       | LIS3MDL_X_AXIS_INTERRUPT
        ------------------------------------------- |           
         0          disabled                        |           0
         1          enabled                         |           1
*/
#define LIS3MDL_X_AXIS_INTERRUPT                                1


/*
        YIEN        Interrupt                       | LIS3MDL_Y_AXIS_INTERRUPT
        ------------------------------------------- |           
         0          disabled                        |           0
         1          enabled                         |           1
*/
#define LIS3MDL_Y_AXIS_INTERRUPT                                1


/*
        ZIEN        Interrupt                       | LIS3MDL_Z_AXIS_INTERRUPT
        ------------------------------------------- |           
         0          disabled                        |           0
         1          enabled                         |           1
*/
#define LIS3MDL_Z_AXIS_INTERRUPT                                1


/*
        IEN         Interrupt                       | LIS3MDL_INTERRUPT_ENABLED
        ------------------------------------------- |           
         0          disabled                        |           0
         1          enabled                         |           1
*/
#define LIS3MDL_INTERRUPT_ENABLED                               0


#endif
