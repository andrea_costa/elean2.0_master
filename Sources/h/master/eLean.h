/*
 * eLean.h
 *
 *  Created on: 18/04/2018
 *      Author: Andrea Costa
 */

#ifndef SOURCES_ELEAN_H_
#define SOURCES_ELEAN_H_

  /* Include modules */
  #include "Cpu.h"
  #include "pin_mux.h"
  #include "dmaController1.h"
  #include "clockMan1.h"
  #include "lpuart1.h"
  #include "Flash.h"
  #include "lpspiCom1.h"
  #include "lpspiCom2.h"
  #include "lpspiCom3.h"
  #include "flexTimer_mc0.h"
  #include "flexTimer_mc1.h"

  /*  */
  #include <string.h>
  #include <stdint.h>
  #include <stdbool.h>
  #include <stdio.h>

  /* Include custom modules */
  #include "HardwareConfig.h"
  #include "iam20680.h"
  #include "lis3mdl.h"
  #include "UartPrint.h"
  #include "CAN_DBC.h"
  #include "FlexCAN_Drv.h"
  #include "FlexCAN_MsgHandler.h"
  #include "FlexCAN_Filters.h"

  #include "interrupts.h"
  #include "middleware.h"

  /* ******************** */
  /* CONFIG STRUCT        */
  /* ******************** */
  typedef struct
  {
    can_baudrate_t  CAN_baudrate;
    uint8_t         CAN_RxFIFO_enabled;
    uint16_t        TRM0_ticks;
    uint16_t        TRM1_ticks;
    uint8_t         iam20680_selftest;
    uint8_t         lis3mdl_selftest;
  } eLean_config_t;

  #include "SystemSetup.h"


  /* ******************** */
  /* DIAGNOSTIC STRUCT    */
  /* ******************** */
  typedef struct
  {
    status_t sbc;
    status_t can0;
    status_t can1;
    status_t can2;
    status_t spi0;
    status_t spi1;
    status_t spi2;
    status_t uart1;
    status_t flash;
  } diagnostics_t;


  /* ******************** */
  /* Generic IC struct    */
  /* ******************** */
  typedef struct
  {
    uint8_t   status;
    uint8_t   error;
    uint8_t   STResults[6];
  } IC_t;


  /* ******************** */
  /* CORE STRUCT          */
  /* ******************** */
  typedef struct
  {
    diagnostics_t diagnostic;
    IC_t          iam20680;
    IC_t          lis3mdl;
    uint8_t       verbose;
  } core_t;




  /* ******************** */
  /* CORE CAN STRUCT      */
  /* ******************** */
  typedef struct
  {
    uint8_t     SpeedCounter;
    uint8_t     Speed_Valid_dataCAN;
    float       SpeedCAN;
    uint8_t     IDModelVCAN;
    uint8_t     DebugModeCAN;
    uint8_t     SendRawDataON;
    uint8_t     SendRawDataOFF;
    uint8_t     ELeanReset;
    uint8_t     EOLRequest;
    uint8_t     EOLResult;
    uint8_t     OBCalibrationReq;
    uint8_t     SendAutoDeoffON;
    uint8_t     SendAutoDeoffOFF;
  } core_can_t;


  extern volatile core_t core;
  extern volatile core_can_t core_can;
  extern flexcan_msgbuf_t flexcan0_msgbuf;
  extern flexcan_msgbuf_t flexcan1_msgbuf;

#endif /* SOURCES_ELEAN_H_ */
