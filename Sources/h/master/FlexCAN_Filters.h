/*
 * FlexCAN_Filters.h
 *
 *  Created on: 03 lug 2018
 *      Author: Andrea_Costa
 */

#ifndef SOURCES_H_APP_FLEXCAN_FILTERS_H_
#define SOURCES_H_APP_FLEXCAN_FILTERS_H_

#define RX_MAILBOXES_MAX_NUMBER       16
#define RxFIFO_FILTER_MAX_NUMBER      8
#define MAILBOX_NOT_USED              255

typedef struct
{
  uint16_t IDs[RX_MAILBOXES_MAX_NUMBER];
  uint8_t  MBs[RX_MAILBOXES_MAX_NUMBER];
} CAN_ID_Filters_t;

/* MBs Filters */
typedef struct
{
  CAN_ID_Filters_t Filters[3];
} CAN_ID_MB_Filters_t;

/* RxFIFO Filters */
typedef struct
{
  CAN_ID_Filters_t Filters[3];
} CAN_ID_RxFIFO_Filters_t;

extern CAN_ID_MB_Filters_t      CAN_ID_MB_Filters;
extern CAN_ID_RxFIFO_Filters_t  CAN_ID_RxFIFO_Filters;


#endif /* SOURCES_H_APP_FLEXCAN_FILTERS_H_ */
