/*
 * middleware.h
 *
 *  Created on: 26 apr 2018
 *      Author: Andrea_Costa
 */

#ifndef SOURCES_H_MIDDLEWARE_H_
#define SOURCES_H_MIDDLEWARE_H_


void TMR0_step (void);

void TMR1_step (void);

#endif /* SOURCES_H_MIDDLEWARE_H_ */
