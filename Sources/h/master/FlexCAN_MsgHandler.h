/*
 * CAN_MsgHandler.h
 *
 *  Created on: 24 apr 2018
 *      Author: Andrea_Costa
 */

#ifndef SOURCES_H_FLEXCAN_MSGHANDLER_H_
#define SOURCES_H_FLEXCAN_MSGHANDLER_H_

void CAN0_MsgHandler (flexcan_msg_t *FlexCan_RxFrame);
void CAN1_MsgHandler (flexcan_msg_t *FlexCan_RxFrame);
void CAN2_MsgHandler (flexcan_msg_t *FlexCan_RxFrame);


uint32_t FlexCAN_TxDebug(uint8_t instance, uint32_t MB, uint16_t msgId);

uint32_t FlexCAN_TxMsg300(uint8_t instance, uint32_t MB, uint16_t msgId);
uint32_t FlexCAN_TxMsg301(uint8_t instance, uint32_t MB, uint16_t msgId);
uint32_t FlexCAN_TxMsg302(uint8_t instance, uint32_t MB, uint16_t msgId);
uint32_t FlexCAN_TxMsg303(uint8_t instance, uint32_t MB, uint16_t msgId);
uint32_t FlexCAN_TxMsg304(uint8_t instance, uint32_t MB, uint16_t msgId);
uint32_t FlexCAN_TxMsg354(uint8_t instance, uint32_t MB, uint16_t msgId);
uint32_t FlexCAN_TxMsg356(uint8_t instance, uint32_t MB, uint16_t msgId);
uint32_t FlexCAN_TxMsg371(uint8_t instance, uint32_t MB, uint16_t msgId);
uint32_t FlexCAN_TxMsg700(uint8_t instance, uint32_t MB, uint16_t msgId);
uint32_t FlexCAN_TxMsg701(uint8_t instance, uint32_t MB, uint16_t msgId);
uint32_t FlexCAN_TxMsg740(uint8_t instance, uint32_t MB, uint16_t msgId);

#endif /* SOURCES_H_CAN_MSGHANDLER_H_ */
