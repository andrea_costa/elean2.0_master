/*
 * middleware.c
 *
 *  Created on: 21 mag 2018
 *      Author: Andrea Costa
 */
#include "eLean.h"

iam20680_data_t    iam20680_data    = {0};


extern uint8_t RTS;

uint16_t TRM0_cnt = 0;
uint16_t TRM1_cnt = 0;

/*!
 \brief TMR0 - Real-time user code
 */
void TMR0_step ()
{
  /* Increase debug timer counter */
  TRM0_cnt++;

  /* Read all data from IMU */
  iam20680_ReadIMU(&iam20680_data);

  if (TRM0_cnt > 1000)
  {
    /* Trigger debug flag - non RT loop purpose */
    RTS       = true;

    /* Reset debug timer counter */
    TRM0_cnt  = 0;
  }
}


/*!
 \brief TMR1 - Real-time user code
 */
void TMR1_step ()
{
  FlexCAN_TxMsg300(0, 16, Tx_CAN_MB_GYRO);
  FlexCAN_TxMsg301(0, 17, Tx_CAN_MB_ACC);
  FlexCAN_TxMsg302(0, 18, Tx_CAN_MB_POSITION);
  FlexCAN_TxMsg303(0, 19, Tx_CAN_MB_CONTROLLER);
  FlexCAN_TxMsg304(0, 20, Tx_CAN_MB_LOCALIZATION);
  FlexCAN_TxMsg354(0, 21, Tx_CAN_HUB_INTERFACE);
  FlexCAN_TxMsg356(0, 22, Tx_CAN_MB_TORQUE);
  FlexCAN_TxMsg371(0, 23, Tx_CAN_VCU_STATE);
  FlexCAN_TxMsg700(0, 24, Tx_CAN_TEST_MESSAGE);
  FlexCAN_TxMsg701(0, 25, Tx_CAN_TEST2_MESSAGE);
  FlexCAN_TxMsg740(0, 26, Tx_CAN_LIDAR_F_SETTING);
}
