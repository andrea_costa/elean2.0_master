/*
 * CAN_MsgHandler.c
 *
 *  Created on: 24 apr 2018
 *      Author: Andrea Costa
 *      Email:  andrea.costa@e-shock.it
 */

#include "eLean.h"


static void CanHandleRxResetPrivateMsg (const flexcan_msg_t *FlexCan_RxFrame);


/*!
 \brief FlexCan0 message parse handler
 */
void CAN0_MsgHandler (flexcan_msg_t *FlexCan_RxFrame)
{

  const flexcan_msg_t * msgReceived = FlexCan_RxFrame;

  switch(msgReceived->msgId)
  {
    case Rx_CAN_HUB_L_DATA:
    {
      break;
    }
    case Rx_CAN_HUB_L_POS_CURR:
    {
      break;
    }
    case Rx_CAN_HUB_R_DATA:
    {
      break;
    }
    case Rx_CAN_HUB_R_POS_CURR:
    {
      break;
    }
    case Rx_CAN_LIDAR_FRONT_DETECTIONS:
    {
      break;
    }
    case Rx_CAN_LIDAR_FRONT_DISTANCE:
    {
      break;
    }
    case Rx_CAN_LIDAR_LEFT_DETECTIONS:
    {
      break;
    }
    case Rx_CAN_LIDAR_LEFT_DISTANCE:
    {
      break;
    }
    case Rx_CAN_LIDAR_REAR_DETECTIONS:
    {
      break;
    }
    case Rx_CAN_LIDAR_REAR_DISTANCE:
    {
      break;
    }
    case Rx_CAN_LIDAR_RIGHT_DETECTIONS:
    {
      break;
    }
    case Rx_CAN_LIDAR_RIGHT_DISTANCE:
    {
      break;
    }
    case Rx_CAN_NAV_U_REF:
    {
      break;
    }
    case Rx_CAN_RESET:
    {
      CanHandleRxResetPrivateMsg(msgReceived);
      break;
    }
    default:
      break;
  }

}


/*!
 \brief FlexCan1 message parse handler
 */
void CAN1_MsgHandler (flexcan_msg_t *FlexCan_RxFrame)
{
  /* Nothing to do here... */
}


/*!
 \brief FlexCan2 message parse handler
 */
void CAN2_MsgHandler (flexcan_msg_t *FlexCan_RxFrame)
{
  /* Nothing to do here... */
}


/*!
 \brief System reset command
 */
static void CanHandleRxResetPrivateMsg (const flexcan_msg_t *FlexCan_RxFrame)
{
  if (FlexCan_RxFrame->data[0] == 0xAA)
    SystemSoftwareReset();
}


/*!
 \brief CAN Out
 */
uint32_t FlexCAN_TxMsg300(uint8_t instance, uint32_t MB, uint16_t msgId)
{
  uint8_t TxCan[8] = {0};
  return FlexCANtransmit(instance, MB, 8, msgId, TxCan);
}


/*!
 \brief CAN Out
 */
uint32_t FlexCAN_TxMsg301(uint8_t instance, uint32_t MB, uint16_t msgId)
{
  uint8_t TxCan[8] = {0};
  return FlexCANtransmit(instance, MB, 8, msgId, TxCan);
}


/*!
 \brief CAN Out
 */
uint32_t FlexCAN_TxMsg302(uint8_t instance, uint32_t MB, uint16_t msgId)
{
  uint8_t TxCan[8] = {0};
  return FlexCANtransmit(instance, MB, 8, msgId, TxCan);
}


/*!
 \brief CAN Out
 */
uint32_t FlexCAN_TxMsg303(uint8_t instance, uint32_t MB, uint16_t msgId)
{
  uint8_t TxCan[8] = {0};
  return FlexCANtransmit(instance, MB, 8, msgId, TxCan);
}


/*!
 \brief CAN Out
 */
uint32_t FlexCAN_TxMsg304(uint8_t instance, uint32_t MB, uint16_t msgId)
{
  uint8_t TxCan[8] = {0};
  return FlexCANtransmit(instance, MB, 8, msgId, TxCan);
}


/*!
 \brief CAN Out
 */
uint32_t FlexCAN_TxMsg354(uint8_t instance, uint32_t MB, uint16_t msgId)
{
  uint8_t TxCan[8] = {0};
  return FlexCANtransmit(instance, MB, 8, msgId, TxCan);
}


/*!
 \brief CAN Out
 */
uint32_t FlexCAN_TxMsg356(uint8_t instance, uint32_t MB, uint16_t msgId)
{
  uint8_t TxCan[8] = {0};
  return FlexCANtransmit(instance, MB, 8, msgId, TxCan);
}


/*!
 \brief CAN Out
 */
uint32_t FlexCAN_TxMsg371(uint8_t instance, uint32_t MB, uint16_t msgId)
{
  uint8_t TxCan[8] = {0};
  return FlexCANtransmit(instance, MB, 8, msgId, TxCan);
}


/*!
 \brief CAN Out
 */
uint32_t FlexCAN_TxMsg700(uint8_t instance, uint32_t MB, uint16_t msgId)
{
  uint8_t TxCan[8] = {0};
  return FlexCANtransmit(instance, MB, 8, msgId, TxCan);
}


/*!
 \brief CAN Out
 */
uint32_t FlexCAN_TxMsg701(uint8_t instance, uint32_t MB, uint16_t msgId)
{
  uint8_t TxCan[8] = {0};
  return FlexCANtransmit(instance, MB, 8, msgId, TxCan);
}


/*!
 \brief CAN Out
 */
uint32_t FlexCAN_TxMsg740(uint8_t instance, uint32_t MB, uint16_t msgId)
{
  uint8_t TxCan[8] = {0};
  return FlexCANtransmit(instance, MB, 8, msgId, TxCan);
}


