/*
 * NVMData.c
 *
 *  Created on: 22 mag 2018
 *      Author: Andrea_Costa
 */

#include "eLean.h"
#include "NVMData.h"

#define NVM_CALIBRATION_SETTINGS_ADDRESS_START  0x0007B000
#define NVM_CALIBRATION_SETTINGS_SECTOR_SIZE    (4u * FEATURE_FLS_PF_BLOCK_SECTOR_SIZE)

#define NVM_ECU_SETTINGS_ADDRESS_START          0x0007F000
#define NVM_ECU_SETTINGS_SECTOR_SIZE            FEATURE_FLS_PF_BLOCK_SECTOR_SIZE

typedef union
{
    struct NVData_EEE_private
    {
        NVData_EEE_t values;
        uint32_t crc;
    } __attribute__ ((__packed__)) data;
    uint8_t rawBytes[sizeof(struct NVData_EEE_private)];
} __attribute__ ((__packed__)) NVData_EEE_private_t;


typedef union
{
    struct NVData_CalibrationSettings_private
    {
        NVData_CalibrationSettings_t values;
        uint32_t crc;
    } __attribute__ ((__packed__)) data;
    uint8_t rawBytes[sizeof(struct NVData_CalibrationSettings_private)];
} __attribute__ ((__packed__)) NVData_CalibrationSettings_private_t;


typedef union
{
    struct NVData_ECUSettings_private
    {
        uint32_t crc;
        NVData_ECUSettings_t values;
    } __attribute__ ((__packed__)) data;
    uint8_t rawBytes[sizeof(struct NVData_ECUSettings_private)];
} __attribute__ ((__packed__)) NVData_ECUSettings_private_t;


static NVData_EEE_private_t                 singletonEEE = {{0}};
static NVData_CalibrationSettings_private_t singletonCalibrationSettings = {{0}};
static NVData_ECUSettings_private_t         singletonECUSettings = {{0}};


extern flash_ssd_config_t flashSSDConfig;


/*!
 \brief Return EEE struct ptr
 */
NVData_EEE_t * MEMORY_Get_NVData_EEE()
{
  return &(singletonEEE.data.values);
}


/*!
 \brief Return CalibrationSettings struct ptr
 */
NVData_CalibrationSettings_t * MEMORY_Get_NVData_CalibrationSettings()
{
  return &(singletonCalibrationSettings.data.values);
}


/*!
 \brief Return CalibrationSettings struct ptr
 */
NVData_ECUSettings_t * MEMORY_Get_NVData_ECUSettings()
{
  return &(singletonECUSettings.data.values);
}


/*!
 \brief Load CalibrationSettings data from P_FLASH to RAM
 */
void MEMORY_Read_NVData_CalibrationSettings()
{
  memcpy(&singletonCalibrationSettings,
         (uint32_t*) NVM_CALIBRATION_SETTINGS_ADDRESS_START,
         sizeof(NVData_CalibrationSettings_private_t));
}


/*!
 \brief Load ECUSettings data from P_FLASH to RAM
 */
void MEMORY_Read_NVData_ECUSettings()
{
  memcpy(&singletonECUSettings,
         (uint32_t*) NVM_ECU_SETTINGS_ADDRESS_START,
         sizeof(NVData_ECUSettings_private_t));
}


/*!
 \brief Read NVData_EEE struct from emulated EEPROM to RAM
 */
void MEMORY_EEE_readData()
{
  /* Try to write data to EEPROM if FlexRAM is configured as EEPROM */
  if (flashSSDConfig.EEESize != 0u)
  {
    memcpy(&singletonEEE,
           (uint32_t*) flashSSDConfig.EERAMBase,
           sizeof(NVData_EEE_private_t));
  }
}


/*!
 \brief Write NVData_EEE struct from RAM to emulated EEPROM
 */
uint32_t MEMORY_EEE_writeData()
{
  return FLASH_DRV_EEEWrite(&flashSSDConfig,
                            (uint32_t*) flashSSDConfig.EERAMBase,
                            sizeof(NVData_EEE_private_t),
                            &singletonEEE);
}

