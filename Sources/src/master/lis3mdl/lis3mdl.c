#include "eLean.h"
#include "lis3mdl_configuration.h"
 
 
#define ABS_F(x) ((x) > 0.0 ? ((long int)x) : -((long int)x))
 
static lis3mdl_t lis3mdl;


lis3mdl_data_t * lis3mdl_GetPtrData() 
{
  return &(lis3mdl.data);
}
 
 
union lis3mdl_status_t * lis3mdl_GetPtrStatus()
{
  return &(lis3mdl.status);
}
 
 
/*!
 \brief lis3mdl - Set register
 */
void lis3mdl_SetReg(uint8_t reg, uint8_t value)
{
  uint8_t cmd[2] = {LIS3MDL_WRITE_REGISTER_BITMASK | reg, value};
  uint8_t resp[2];
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM3, &cmd, &resp, 2, 1000);
}
 

/*!
 \brief lis3mdl - Get register value
 */
uint8_t lis3mdl_GetReg(uint8_t reg)
{
  uint8_t cmd[2] = {LIS3MDL_READ_REGISTER_BITMASK | reg, 0};
  uint8_t resp[2];
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM3, &cmd, &resp, 2, 1000);
  return resp[1];
}

 
/*!
 \brief lis3mdl - Read device id
 */
void lis3mdl_device_id(lis3mdl_data_t *data)
{
  uint8_t cmd[2] = {LIS3MDL_READ_REGISTER_BITMASK | LIS3MDL_WHO_AM_I, 0};
  uint8_t resp[2] = {0};
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM3, &cmd, &resp, 2, 1000);

  data->device_id = resp[1];
}
 

/*!
 \brief lis3mdl - Get IC status
 */
void lis3mdl_GetStatus(lis3mdl_t *lis3mdl)
{
  uint8_t cmd[2] = {LIS3MDL_READ_REGISTER_BITMASK | LIS3MDL_STATUS_REG, 0};
  uint8_t resp[2];
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM3, &cmd, &resp, 2, 1000);
  lis3mdl->status.value = resp[1];
}
 
/*!
 \brief lis3mdl - Read status
 */
void lis3mdl_ReadStatus()
{
  uint8_t cmd[2] = {LIS3MDL_READ_REGISTER_BITMASK | LIS3MDL_STATUS_REG, 0};
  uint8_t resp[2];
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM3, &cmd, &resp, 2, 1000);
  lis3mdl.status.value = resp[1];
}
 

/*!
 \brief lis3mdl - Get register
 */
void lis3mdl_GetData (lis3mdl_data_t *data)
{
  uint8_t cmd[6]      = {LIS3MDL_READ_MULTI_REGISTER_BITMASK | LIS3MDL_OUT_X_L, 0, 0, 0, 0, 0};
  uint8_t resp[6]     = {0};
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM3, &cmd, &resp, 7, 1000);

  data->x             = (int16_t) (resp[1] & 0x00FF)  | ((resp[2] & 0x00FF) << 8);
  data->y             = (int16_t) (resp[3] & 0x00FF)  | ((resp[4] & 0x00FF) << 8);
  data->z             = (int16_t) (resp[5] & 0x00FF)  | ((resp[6] & 0x00FF) << 8);
}


/*!
 \brief lis3mdl - Get data and temperature
 */
void lis3mdl_GetDataAndTemp (lis3mdl_data_t *data)
{
  uint8_t cmd[6]      = {LIS3MDL_READ_MULTI_REGISTER_BITMASK | LIS3MDL_OUT_X_L, 0, 0, 0, 0, 0};
  uint8_t resp[6]     = {0};
  uint8_t temp_h[2] = {0}, temp_l[2] = {0};
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM3, &cmd, &resp, 7, 1000);

  data->x             = (int16_t) (resp[1] & 0x00FF)  | ((resp[2] & 0x00FF) << 8);
  data->y             = (int16_t) (resp[3] & 0x00FF)  | ((resp[4] & 0x00FF) << 8);
  data->z             = (int16_t) (resp[5] & 0x00FF)  | ((resp[6] & 0x00FF) << 8);

  cmd[0]      = LIS3MDL_READ_MULTI_REGISTER_BITMASK | LIS3MDL_TEMP_OUT_L;
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM3, &cmd, &temp_l, 2, 1000);

  cmd[0]      = LIS3MDL_READ_MULTI_REGISTER_BITMASK | LIS3MDL_TEMP_OUT_H;
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM3, &cmd, &temp_h, 2, 1000);

  data->temperature   = (int16_t) (temp_l[1] & 0x00FF)  | ((temp_h[1] & 0x00FF) << 8);
}


/*!
 \brief lis3mdl - Read data and temperature
 */
void lis3mdl_ReadData ()
{
  uint8_t cmd[7]      = {LIS3MDL_READ_MULTI_REGISTER_BITMASK | LIS3MDL_OUT_X_L, 0, 0, 0, 0, 0, 0};
  uint8_t resp[7]     = {0};
  uint8_t temp_h[2] =   {0}, temp_l[2] = {0};
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM3, &cmd, &resp, 7, 1000);

  lis3mdl.data.x             = (int16_t) (resp[1] & 0x00FF)  | ((resp[2] & 0x00FF) << 8);
  lis3mdl.data.y             = (int16_t) (resp[3] & 0x00FF)  | ((resp[4] & 0x00FF) << 8);
  lis3mdl.data.z             = (int16_t) (resp[5] & 0x00FF)  | ((resp[6] & 0x00FF) << 8);

  cmd[0] = LIS3MDL_READ_MULTI_REGISTER_BITMASK | LIS3MDL_TEMP_OUT_L;
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM3, &cmd, &temp_l, 2, 1000);

  cmd[0] = LIS3MDL_READ_MULTI_REGISTER_BITMASK | LIS3MDL_TEMP_OUT_H;
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM3, &cmd, &temp_h, 2, 1000);

  lis3mdl.data.temperature   = (int16_t) (temp_l[1] & 0x00FF)  | ((temp_h[1] & 0x00FF) << 8);
}
 

/*!
 \brief lis3mdl - Reset device
 */
void lis3mdl_Reset()
{
  uint8_t cmd[2]      = {LIS3MDL_READ_REGISTER_BITMASK | LIS3MDL_CTRL_REG2, 0};
  uint8_t resp        = {0};

  /* Get CTRL_REG2 */
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM3, &cmd, &resp, 1, 1000);

  /* Set SOFT_RST bit */
  resp |= 1 << 2;

  cmd[0] = LIS3MDL_WRITE_REGISTER_BITMASK | LIS3MDL_CTRL_REG2;
  cmd[1] = resp;

  LPSPI_DRV_MasterTransferBlocking(LPSPICOM3, &cmd, &resp, 2, 1000);
}


/*!
 \brief lis3mdl - Init IC
 */
uint8_t LIS3MDL_Init()
{
  lis3mdl_data_t data;

  lis3mdl_device_id (&data);

  if (LIS3MDL_ID_VAL != (data.device_id & 0x00FF))
      return LIS3MDL_READ_ID_ERROR;

  /* Get configuration from lis3mdl_configurations.h */
  lis3mdl_InitConfigVariables();

  lis3mdl_Reset();

  OSIF_TimeDelay(50);
     
  /* Configure CTRL_REG1 (20h)
      TEMP_EN[7]      : 0 temp sensor disabled, 1 temp sensor enabled
      OM[6:5]         : X and Y axes operative mode selection. Def value 00
      DO[4:2]         : Output data rate selection. Def value 100
      FAST_ODR[1]     : 0 Fast_ODR disabled, 1 FAST_ODR enabled
      ST[0]           : 0 self-test disabled, 1 self-test enabled
  */
  lis3mdl_SetReg(LIS3MDL_CTRL_REG1, lis3mdl.reg1.value);

  /*
      CTRL_REG2 (21h)
      0[7]            : set to 0 for correct functioning of the device
      FS0[6:5]        : Full-scale configuration. Def value +-2 Gauss
      0[4]            : set to 0 for correct functioning of the device
      REBOOT[3]       : 0 normal mode, 1 reboot memory content
      SOFT_RST[2]     : 0 Default value, 1 Reset operation
      0[1]            : set to 0 for correct functioning of the device
      0[0]            : set to 0 for correct functioning of the device
  */
  lis3mdl_SetReg(LIS3MDL_CTRL_REG2, lis3mdl.reg2.value);
 
  /*
      CTRL_REG3 (22h)
      0[7:6]          : set to 0 for correct functioning of the device
      LO[5]           : Low-power mode configuration. Def value 0
      0[4:3]          : set to 0 for correct functioning of the device
      SIM[2]          : 0 4-wire interface, 1 3-wire interface
      MD[1:0]         : Operating mode selection. Def value: 11
  */
  lis3mdl_SetReg(LIS3MDL_CTRL_REG3, lis3mdl.reg3.value);

  /*
      CTRL_REG4 (23h)
      0[7:4]          : set to 0 for correct functioning of the device
      OMZ[2:3]        : Z-axis operative mode selection
      BLE[1]          : 0 data LSb at lower address; 1 data MSb at lower address. Def value 0
      0[0]            : set to 0 for correct functioning of the device
  */
  lis3mdl_SetReg(LIS3MDL_CTRL_REG4, lis3mdl.reg4.value);

  /*
      CTRL_REG5 (24h)
      FAST_READ[7]    : 0 FAST_READ disabled; 1 FAST_READ enabled. Def value 0
      BDU[6]          : Block data update for magnetic data, Def value 0 (continuos sample)
      0[5:0]          : 0 data LSb at lower address; 1 data MSb at lower address. Def value 0
  */
  lis3mdl_SetReg(LIS3MDL_CTRL_REG5, lis3mdl.reg5.value);
 
  /*
      STATUS_REG (27h)
      ZYXOR[7]        : X-, Y- and Z-axis data overrun. Def value 0
      ZOR[6]          : Z-axis data overrun. Def value 0
      YOR[5]          : Y-axis data overrun. Def value 0
      XOR[4]          : X-axis data overrun. Def value 0
      ZYXDA[3]        : X-, Y- and Z-axis new data available. Def value 0
      ZDA[2]          : Z-axis new data available. Def value 0
      YDA[1]          : Y-axis new data available. Def value 0
      XDA[0]          : X-axis new data available. Def value 0
  */

  /*
      INT_CFG (30h)
      XIEN[7]         : Enable interrupt generation on X-axis. Def value: 1
      YIEN[6]         : Enable interrupt generation on Y-axis. Def value: 1
      ZIEN[5]         : Enable interrupt generation on Z-axis. Def value: 1
      0[4]            :
      1[3]            :
      IEA[2]          : Interrupt active configuration on INT. Def value 0
      LIR[1]          : Latch interrupt request. Def value 0
      IEN[0]          : Interrupt enable on INT pin. Def value 0
  */
  lis3mdl_SetReg(LIS3MDL_INT_CFG, lis3mdl.int_cfg.value);
 
  /*
      INT_CFG (31h)
      PTH_X[7]        : Value on X-axis exceeds the threshold on the positive side
      PTH_Y[6]        : Value on Y-axis exceeds the threshold on the positive side
      PTH_Z[5]        : Value on Z-axis exceeds the threshold on the positive side
      NTH_X[4]        : Value on X-axis exceeds the threshold on the negative side
      NTH_Y[3]        : Value on Y-axis exceeds the threshold on the negative side
      NTH_Z[2]        : Value on Z-axis exceeds the threshold on the negative side
      MROI[1]         : Internal measurement range overflow on magnetic value
      INT[0]          : This bit signals when an interrupt event occurs
  */
  lis3mdl_SetReg(LIS3MDL_INT_SRC, lis3mdl.int_src.value);

  return LIS3MDL_NO_ERROR;
}


/*!
 \brief lis3mdl - Perform device selftest
 */
uint8_t LIS3MDL_SelfTest(uint8_t selftest)
{
  if (selftest == false)
    return LIS3MDL_NO_ERROR;

  lis3mdl_data_t  data;
  long int    x_NormalMean = 0.0, y_NormalMean = 0.0, z_NormalMean = 0.0;
  long int    x_STMean = 0.0, y_STMean = 0.0, z_STMean = 0.0;
  long int    x_diff = 0.0, y_diff = 0.0, z_diff = 0.0;
  uint8_t     ret = LIS3MDL_NO_ERROR;
  uint16_t    freeze_cnt = 0;
  uint16_t    freeze_cnt_max = 10000;
     
  /* Temperature sensor disabled, X-Y Low-power mode, ODR 80 Hz, Fast_ODR disabled, no ST */
  lis3mdl_SetReg(LIS3MDL_CTRL_REG1, 0x1C);

  /* �12 gauss, no reboot, no soft_rst */
  lis3mdl_SetReg(LIS3MDL_CTRL_REG2, 0x40);

  OSIF_TimeDelay(30);

  /* Continuos mode, 4-wire interface, no LP */
  lis3mdl_SetReg(LIS3MDL_CTRL_REG3, 0x00);

  lis3mdl_GetStatus(&lis3mdl);

  OSIF_TimeDelay(30);
     
  /* Wait for the Data Ready Bit */
  lis3mdl_GetStatus(&lis3mdl);

  while(lis3mdl.status.bits.ZYXDA == 0)
  {
    lis3mdl_GetStatus(&lis3mdl);
    freeze_cnt++;

    if (freeze_cnt > freeze_cnt_max)
      return false;
  }
     
  freeze_cnt = 0;

  /* Discart first data */
  lis3mdl_GetData(&data);

  uint8_t i;
     
  /* Read 5 samples */
  for (i = 0; i < 5; i++)
  {
    lis3mdl_GetStatus(&lis3mdl);

    while(lis3mdl.status.bits.ZYXDA == 0)
    {
      lis3mdl_GetStatus(&lis3mdl);
      freeze_cnt++;

      if (freeze_cnt > freeze_cnt_max)
          return false;
    }
     
    freeze_cnt = 0;

    lis3mdl_GetData(&data);

    x_NormalMean = x_NormalMean + (long int) data.x;
    y_NormalMean = y_NormalMean + (long int) data.y;
    z_NormalMean = z_NormalMean + (long int) data.z;
  }
     
  x_NormalMean = x_NormalMean / 5;
  y_NormalMean = y_NormalMean / 5;
  z_NormalMean = z_NormalMean / 5;

  /*******************/
  /* Enable sel test */
  /*******************/
  lis3mdl_SetReg(LIS3MDL_CTRL_REG1, 0x1D);
  /* Continuos mode, 4-wire interface, no LP */
  lis3mdl_SetReg(LIS3MDL_CTRL_REG3, 0x00);

  OSIF_TimeDelay(60);
     
  /* Wait for the Data Ready Bit */
  lis3mdl_GetStatus(&lis3mdl);

  while(lis3mdl.status.bits.ZYXDA == 0)
  {
    lis3mdl_GetStatus(&lis3mdl);
    freeze_cnt++;

    if (freeze_cnt > freeze_cnt_max)
      return LIS3MDL_GENERIC_ERROR;
  }
     
  freeze_cnt = 0;

  /* Discart first data */
  lis3mdl_GetData(&data);

  /* Read 5 samples */
  for (i = 0; i < 5; i++)
  {
    lis3mdl_GetStatus(&lis3mdl);

    while(lis3mdl.status.bits.ZYXDA == 0)
    {
      /* NB - il processo si blocca qui! */
      lis3mdl_GetStatus(&lis3mdl);
      freeze_cnt++;

      if (freeze_cnt > freeze_cnt_max)
        return LIS3MDL_GENERIC_ERROR;
    }

    freeze_cnt = 0;

    lis3mdl_GetData(&data);
     
    x_STMean = x_STMean + (long int) data.x;
    y_STMean = y_STMean + (long int) data.y;
    z_STMean = z_STMean + (long int) data.z;
  }
     
  x_STMean = x_STMean / 5;
  y_STMean = y_STMean / 5;
  z_STMean = z_STMean / 5;

  /* Calculate mean difference */
  x_diff = ABS_F(x_STMean - x_NormalMean);
  y_diff = ABS_F(y_STMean - y_NormalMean);
  z_diff = ABS_F(z_STMean - z_NormalMean);

  if ((x_diff < MIN_ST_X) || (x_diff > MAX_ST_X))
    ret = LIS3MDL_NO_ERROR;

  if ((y_diff < MIN_ST_Y) || (y_diff > MAX_ST_Y))
    ret = LIS3MDL_NO_ERROR;

  if ((z_diff < MIN_ST_Z) || (z_diff > MAX_ST_Z))
    ret = LIS3MDL_NO_ERROR;
     
  /* Disable self-test */
  lis3mdl_SetReg(LIS3MDL_CTRL_REG1, 0x1C);

  return ret;
}
 
 
void lis3mdl_InitConfigVariables()
{
    /* REG1 */
    switch(LIS3MDL_TEMPERATURE_SENSOR_ENABLED)
    {
        case 0:
            lis3mdl.reg1.bits.TEMP_EN = 0;
            break;
             
        case 1:
            lis3mdl.reg1.bits.TEMP_EN = 1;
            break;
             
        default:
            lis3mdl.reg1.bits.TEMP_EN = 1;
            break;
    }
             
    switch(LIS3MDL_XY_OPERATION_MODE_CONFIG)
    {
        case 0:
            lis3mdl.reg1.bits.OM = 0;
            break;
        case 1:
            lis3mdl.reg1.bits.OM = 1;
            break;
             
        case 2:
            lis3mdl.reg1.bits.OM = 2;
            break;
             
        case 3:
            lis3mdl.reg1.bits.OM = 3;
            break;
 
        default:
            lis3mdl.reg1.bits.OM = 3;
            break;
    }
     
    switch (LIS3MDL_ODR)
    {
        case 0:
            lis3mdl.reg1.bits.DO = 0;
            break;
        case 1:
            lis3mdl.reg1.bits.DO = 1;
            break;
             
        case 2:
            lis3mdl.reg1.bits.DO = 2;
            break;
             
        case 3:
            lis3mdl.reg1.bits.DO = 3;
            break;
             
        case 4:
            lis3mdl.reg1.bits.DO = 4;
            break;
             
        case 5:
            lis3mdl.reg1.bits.DO = 5;
            break;
             
        case 6:
            lis3mdl.reg1.bits.DO = 6;
            break;
             
        case 7:
            lis3mdl.reg1.bits.DO = 7;
            break;
 
        default:
            lis3mdl.reg1.bits.DO = 3;
            break;
    }
     
     
    switch (LIS3MDL_ODR_FAST)
    {
        case 0:
            lis3mdl.reg1.bits.FAST_ODR = 0;
            break;
        case 1:
            lis3mdl.reg1.bits.FAST_ODR = 1;
            break;
             
        default:
            lis3mdl.reg1.bits.FAST_ODR = 0;
            break;
    }
     
     
    /* REG2 - Set default values */
    lis3mdl.reg2.bits.pad0      = 0;
    lis3mdl.reg2.bits.pad1      = 0;
    lis3mdl.reg2.bits.pad4      = 0;
    lis3mdl.reg2.bits.pad7      = 0;
    lis3mdl.reg2.bits.REBOOT    = 0;
    lis3mdl.reg2.bits.SOFT_RST  = 0;
     
    switch(LIS3MDL_FULL_SCALE)
    {
        case 0:
            lis3mdl.reg2.bits.FS = 0;
            break;
 
        case 1:
            lis3mdl.reg2.bits.FS = 1;
            break;
             
        case 2:
            lis3mdl.reg2.bits.FS = 2;
            break;
 
        case 3:
            lis3mdl.reg2.bits.FS = 3;
            break;
 
        default:
            lis3mdl.reg2.bits.FS = 0;
            break;
    }
     
    /* REG2 - Set default values */
    lis3mdl.reg3.bits.pad3      = 0;
    lis3mdl.reg3.bits.pad4      = 0;
    lis3mdl.reg3.bits.pad6      = 0;
    lis3mdl.reg3.bits.pad7      = 0;
     
    switch(LIS3MDL_SPI_CONF)
    {
        case 0:
            lis3mdl.reg3.bits.sim = 0;
            break;
 
        case 1:
            lis3mdl.reg3.bits.sim = 1;
            break;
 
        default:
            lis3mdl.reg3.bits.sim = 0;
            break;
    }
     
 
    switch(LIS3MDL_OPERATION_MODE)
    {
        case 0:
            lis3mdl.reg3.bits.MD = 0;
            break;
             
        case 1:
            lis3mdl.reg3.bits.MD = 1;
            break;
             
        case 2:
            lis3mdl.reg3.bits.MD = 2;
            break;
 
        case 3:
            lis3mdl.reg3.bits.MD = 3;
            break;
             
        default:
            lis3mdl.reg3.bits.MD = 0;
            break;
    }
     
     
    switch(LIS3MDL_LOW_POWER)
    {
        case 0:
            lis3mdl.reg3.bits.LP = 0;
            break;
             
        case 1:
            lis3mdl.reg3.bits.LP = 1;
            break;
             
        default:
            lis3mdl.reg3.bits.LP = 0;
            break;
    }
     
    /* REG4 - Set default values */
    lis3mdl.reg4.bits.pad0      = 0;
    lis3mdl.reg4.bits.pad4      = 0;
    lis3mdl.reg4.bits.pad5      = 0;
    lis3mdl.reg4.bits.pad6      = 0;
    lis3mdl.reg4.bits.pad7      = 0;
    lis3mdl.reg4.bits.BLE       = 0;
     
    switch(LIS3MDL_Z_OPERATION_MODE_CONFIG)
    {
        case 0:
            lis3mdl.reg4.bits.OMZ = 0;
            break;
        case 1:
            lis3mdl.reg4.bits.OMZ = 1;
            break;
             
        case 2:
            lis3mdl.reg4.bits.OMZ = 2;
            break;
             
        case 3:
            lis3mdl.reg4.bits.OMZ = 3;
            break;
 
        default:
            lis3mdl.reg4.bits.OMZ = 3;
            break;
    }
     
     
    /* INT_CFG - Set default values */
    lis3mdl.int_cfg.bits.force3 = 1;
    lis3mdl.int_cfg.bits.pad4   = 0;
     
    switch(LIS3MDL_X_AXIS_INTERRUPT)
    {
        case 0:
            lis3mdl.int_cfg.bits.XIEN = 0;
            break;
        case 1:
            lis3mdl.int_cfg.bits.XIEN = 1;
            break;
        default:
            lis3mdl.int_cfg.bits.XIEN = 1;
            break;
    }
     
     
    switch(LIS3MDL_Y_AXIS_INTERRUPT)
    {
        case 0:
            lis3mdl.int_cfg.bits.YIEN = 0;
            break;
        case 1:
            lis3mdl.int_cfg.bits.YIEN = 1;
            break;
        default:
            lis3mdl.int_cfg.bits.YIEN = 1;
            break;
    }
     
 
    switch(LIS3MDL_Z_AXIS_INTERRUPT)
    {
        case 0:
            lis3mdl.int_cfg.bits.ZIEN = 0;
            break;
        case 1:
            lis3mdl.int_cfg.bits.ZIEN = 1;
            break;
        default:
            lis3mdl.int_cfg.bits.ZIEN = 1;
            break;
    }
 
     
    switch(LIS3MDL_INTERRUPT_ENABLED)
    {
        case 0:
            lis3mdl.int_cfg.bits.IEN = 0;
            break;
        case 1:
            lis3mdl.int_cfg.bits.IEN = 1;
            break;
        default:
            lis3mdl.int_cfg.bits.IEN = 0;
            break;
    }
}
