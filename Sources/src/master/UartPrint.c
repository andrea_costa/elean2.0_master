/*
 * UartPrint.c
 *
 *  Created on: 15 giu 2018
 *      Author: Andrea_Costa
 */

#include "eLean.h"

void print(char *str)
{
  uint32_t bytesRemaining;

  LPUART_DRV_SendData(INST_LPUART1, (uint8_t *)str, strlen(str));
  /* Wait for transmission to be complete */
  while(LPUART_DRV_GetTransmitStatus(INST_LPUART1, &bytesRemaining) != STATUS_SUCCESS);
}
