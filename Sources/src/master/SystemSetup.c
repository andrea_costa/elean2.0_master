/*
 * SystemSetup.c
 *
 *  Created on: 18/04/2018
 *      Author: Andrea Costa
 */

#include "eLean.h"


/* Timer state structs */
ftm_state_t ftmMC0StateStruct;
ftm_state_t ftmMC1StateStruct;

/* Flash config struct */
flash_ssd_config_t flashSSDConfig;


/*!
 \brief Enable on-device FPU
 */
void enableFPU(void)
{
  /* Enable FPU - set both CPACR[CP11] and CPACR[CP10] to Full Access - 0b11 */
  S32_SCB->CPACR |= (S32_SCB_CPACR_CP10_MASK | S32_SCB_CPACR_CP11_MASK);
}


/*!
 \brief Main system setup
 */
void SystemSetup(eLean_config_t config)
{
  /* Enable the floating point unit */
  enableFPU();

  /* Initialize and configure clocks */
  CLOCK_SYS_Init( g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT,
                  g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
  CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);

  /* Initialize pins */
  PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

  /* Initialize LPUART instance */
  core.diagnostic.uart1 = LPUART_DRV_Init(INST_LPUART1, &lpuart1_State, &lpuart1_InitConfig0);

  /* Initialize SPI0 instance - SBC */
  core.diagnostic.spi0  = LPSPI_DRV_MasterInit(LPSPICOM1, &lpspiCom1State, &lpspiCom1_MasterConfig0);

  /* Initialize SPI1 instance - IAM20680 */
  core.diagnostic.spi1  = LPSPI_DRV_MasterInit(LPSPICOM2, &lpspiCom2State, &lpspiCom2_MasterConfig0);

  /* Initialize SPI2 instance - LIS3MDL */
  core.diagnostic.spi2  = LPSPI_DRV_MasterInit(LPSPICOM3, &lpspiCom3State, &lpspiCom3_MasterConfig0);

  /* Initialize TMR0 (MC0) instance - Stopped */
  flexTimer_mc0(2, false, config.TRM0_ticks);

  /* Initialize TMR1 (MC1) instance - Stopped */
  flexTimer_mc1(2, false, config.TRM1_ticks);

  if (config.CAN_RxFIFO_enabled)
  {
    /* Initialize FlexCAN driver with RxFIFO (instance, MB used, baudrate) */
    core.diagnostic.can0 = FlexCANInitRxFIFO(0, 32, config.CAN_baudrate);
  }
  else
  {
    /* Initialize FlexCAN driver with RxFIFO (instance, MB used, baudrate) */
    core.diagnostic.can0 = FlexCANInit(0, 32, config.CAN_baudrate);
  }
}


/*!
 \brief FlexTimer setup
 */
void flexTimer_mc0(uint8_t priority, uint8_t startTimer, uint16_t ticks)
{
  /* Initialize Flex Timer instance as simple timer */
  FTM_DRV_Init(INST_FLEXTIMER_MC0, &flexTimer_mc0_InitConfig, &ftmMC0StateStruct);

  /* Install handler for the Timer overflow interrupt and enable it */
  INT_SYS_InstallHandler(FTM0_Ovf_Reload_IRQn, &ftmTimerMC0ISR, (isr_t*) 0);
  INT_SYS_SetPriority(FTM0_Ovf_Reload_IRQn, priority);
  INT_SYS_EnableIRQ(FTM0_Ovf_Reload_IRQn);

  /* Update timer ticks */
  flexTimer_mc0_TimerConfig.finalValue = ticks;

  /* Setup the counter to trigger an interrupt every -ticks- tick */
  FTM_DRV_InitCounter(INST_FLEXTIMER_MC0, &flexTimer_mc0_TimerConfig);

  if (startTimer == true)
  {
    /* Start the counter */
    FTM_DRV_CounterStart(INST_FLEXTIMER_MC0);
  }
}

/*!
 \brief FlexTimer setup
 */
void flexTimer_mc1(uint8_t priority, uint8_t startTimer, uint16_t ticks)
{
  /* Initialize Flex Timer instance as simple timer */
  FTM_DRV_Init(INST_FLEXTIMER_MC1, &flexTimer_mc1_InitConfig, &ftmMC1StateStruct);

  /* Install handler for the Timer overflow interrupt and enable it */
  INT_SYS_InstallHandler(FTM1_Ovf_Reload_IRQn, &ftmTimerMC1ISR, (isr_t*) 0);
  INT_SYS_SetPriority(FTM1_Ovf_Reload_IRQn, priority);
  INT_SYS_EnableIRQ(FTM1_Ovf_Reload_IRQn);

  /* Update timer ticks */
  flexTimer_mc1_TimerConfig.finalValue = ticks;

  /* Setup the counter to trigger an interrupt every -ticks- tick */
  FTM_DRV_InitCounter(INST_FLEXTIMER_MC1, &flexTimer_mc1_TimerConfig);

  if (startTimer == true)
  {
    /* Start the counter */
    FTM_DRV_CounterStart(INST_FLEXTIMER_MC1);
  }
}


/*!
 \brief FlexTimers start/stop
 */
void flexTimer_start(uint8_t TMR0, uint8_t TMR1)
{
  if (TMR0 == true)
  {
    /* Start TMR0 (MC1) */
    FTM_DRV_CounterStart(INST_FLEXTIMER_MC0);
  }

  if (TMR1 == true)
  {
    /* Start TMR1 (MC2) */
    FTM_DRV_CounterStart(INST_FLEXTIMER_MC1);
  }
}


/*!
 \brief FlexTimers stop
 */
void flexTimer_stop(uint8_t TMR0, uint8_t TMR1)
{
  if (TMR0 == true)
  {
    /* Start TMR0 (MC1) */
    FTM_DRV_CounterStop(INST_FLEXTIMER_MC0);
  }

  if (TMR1 == true)
  {
    /* Start TMR1 (MC2) */
    FTM_DRV_CounterStop(INST_FLEXTIMER_MC1);
  }
}


/*!
 \brief UJA1169 Setup
 */
uint32_t SBCSetup(void)
{
  /* Initialize SBC - uja1169 */
  core.diagnostic.sbc   = SBC_Init(&sbc_uja11691_InitConfig0, LPSPICOM1);

  /* Unlock SBC if needed - by default is in FCNM */
  sbc_factories_conf_t factoriesConf;
  SBC_GetFactoriesSettings(&factoriesConf);

  if (  factoriesConf.control.fnmc == SBC_UJA_SBC_FNMC_EN ||
        factoriesConf.control.sdmc == SBC_UJA_SBC_SDMC_EN ||
        factoriesConf.control.slpc == SBC_UJA_SBC_SLPC_IG
     )
  {
    if (SBC_RecoverFromFNMC() != STATUS_SUCCESS)
    {
      print("# Error: SBC_RecoverFromFNMC() failed.\n");
      return STATUS_ERROR;
    }
  }

  if (SBC_Configure() != STATUS_SUCCESS)
  {
    print("# Error: SBC_Configure() failed.\n");
    return STATUS_ERROR;
  }

  //uint8_t data_out, sbc_model;
  //SBC_DataTransfer(SBC_UJA_IDENTIF, &data_out, &sbc_model);

  return STATUS_SUCCESS;
}


/*!
 \brief UJA1169 - Recover from FNMC
 */
uint32_t SBC_RecoverFromFNMC()
{
  sbc_factories_conf_t factoriesConf;

  /* Set correct settings */
  factoriesConf.control.fnmc = SBC_UJA_SBC_FNMC_DIS;
  factoriesConf.control.sdmc = SBC_UJA_SBC_SDMC_DIS;
  factoriesConf.control.slpc = SBC_UJA_SBC_SLPC_AC;

  if (SBC_ChangeFactoriesSettings(&factoriesConf) != STATUS_SUCCESS)
    return STATUS_ERROR;

  return STATUS_SUCCESS;
}


/*!
 \brief UJA1169 - Configure
 */
uint32_t SBC_Configure()
{
  if (SBC_SetMode(SBC_UJA_MODE_MC_NORMAL) != STATUS_SUCCESS)
    return STATUS_ERROR;

  sbc_can_conf_t config;
  config.canConf.cfdc       = SBC_UJA_CAN_CFDC_DIS;
  config.canConf.cmc        = SBC_UJA_CAN_CMC_ACMODE_DD;
  config.canConf.cpnc       = SBC_UJA_CAN_CPNC_EN;
  config.canTransEvnt.cbse  = SBC_UJA_TRANS_EVNT_CBSE_DIS;
  config.canTransEvnt.cfe   = SBC_UJA_TRANS_EVNT_CFE_EN;
  config.canTransEvnt.cwe   = SBC_UJA_TRANS_EVNT_CWE_EN;
  config.datRate            = SBC_UJA_DAT_RATE_CDR_500KB;

  if (SBC_SetCanConfig(&config) != STATUS_SUCCESS)
    return STATUS_ERROR;

  /* Check if the SBC is in NORMAL MODE, if NOT -> error */
  sbc_mode_mc_t mode;

  if (SBC_GetMode(&mode) != STATUS_SUCCESS)
      return STATUS_ERROR;

  SBC_GetCanConfig(&config);

  /* DEBUG INFO
  sprintf(TXbuf, "SBC Mode: %x\n", mode);
  print(TXbuf);

  sprintf(TXbuf, "SBC cmc: %x\n", config.canConf.cmc);
  print(TXbuf);
  */

  if (mode != SBC_UJA_MODE_MC_NORMAL)
    return STATUS_ERROR;

  return STATUS_SUCCESS;
}


uint32_t NVMSetup()
{
  status_t ret;        /* Store the driver APIs return code */
  uint32_t address;
  uint32_t size;

#if (FEATURE_FLS_HAS_PROGRAM_PHRASE_CMD == 1u)
  uint8_t unsecure_key[FTFx_PHRASE_SIZE] = {0xFFu, 0xFFu, 0xFFu, 0xFFu, 0xFEu, 0xFFu, 0xFFu, 0xFFu};
#else   /* FEATURE_FLASH_HAS_PROGRAM_LONGWORD_CMD */
  uint8_t unsecure_key[FTFx_LONGWORD_SIZE] = {0xFEu, 0xFFu, 0xFFu, 0xFFu};
#endif  /* FEATURE_FLS_HAS_PROGRAM_PHRASE_CMD */

  /* Always initialize the driver before calling other functions */
  ret = FLASH_DRV_Init(&Flash_InitConfig0, &flashSSDConfig);
  DEV_ASSERT(STATUS_SUCCESS == ret);


#if ((FEATURE_FLS_HAS_FLEX_NVM == 1u) & (FEATURE_FLS_HAS_FLEX_RAM == 1u))
    /* Config FlexRAM as EEPROM if it is currently used as traditional RAM */
    if (flashSSDConfig.EEESize == 0u)
    {
#ifndef FLASH_TARGET
      /* First, erase all Flash blocks if code is placed in RAM to ensure
       * the IFR region is blank before partitioning FLexNVM and FlexRAM */
      ret = FLASH_DRV_EraseAllBlock(&flashSSDConfig);
      DEV_ASSERT(STATUS_SUCCESS == ret);

      /* Verify the erase operation at margin level value of 1 */
      ret = FLASH_DRV_VerifyAllBlock(&flashSSDConfig, 1u);
      DEV_ASSERT(STATUS_SUCCESS == ret);

      /* Reprogram secure byte in Flash configuration field */
#if (FEATURE_FLS_HAS_PROGRAM_PHRASE_CMD == 1u)
      address = 0x408u;
      size = FTFx_PHRASE_SIZE;
#else   /* FEATURE_FLASH_HAS_PROGRAM_LONGWORD_CMD == 1u */
      address = 0x40Cu;
      size = FTFx_LONGWORD_SIZE;
#endif /* FEATURE_FLS_HAS_PROGRAM_PHRASE_CMD */
      ret = FLASH_DRV_Program(&flashSSDConfig, address, size, unsecure_key);
      DEV_ASSERT(STATUS_SUCCESS == ret);
#endif /* FLASH_TARGET */

      /* Configure FlexRAM as EEPROM and FlexNVM as EEPROM backup region,
       * DEFlashPartition will be failed if the IFR region isn't blank.
       * Refer to the device document for valid EEPROM Data Size Code
       * and FlexNVM Partition Code. For example on S32K144:
       * - EEEDataSizeCode = 0x02u: EEPROM size = 4 Kbytes
       * - DEPartitionCode = 0x08u: EEPROM backup size = 64 Kbytes */
      ret = FLASH_DRV_DEFlashPartition(&flashSSDConfig, 0x02u, 0x08u, 0x0u, false, true);
      DEV_ASSERT(STATUS_SUCCESS == ret);

      /* Re-initialize the driver to update the new EEPROM configuration */
      ret = FLASH_DRV_Init(&Flash_InitConfig0, &flashSSDConfig);
      DEV_ASSERT(STATUS_SUCCESS == ret);

      /* Make FlexRAM available for EEPROM */
      ret = FLASH_DRV_SetFlexRamFunction(&flashSSDConfig, EEE_ENABLE, 0x00u, NULL);
      DEV_ASSERT(STATUS_SUCCESS == ret);
    }
    else    /* FLexRAM is already configured as EEPROM */
    {
      /* Make FlexRAM available for EEPROM, make sure that FlexNVM and FlexRAM
       * are already partitioned successfully before */
      ret = FLASH_DRV_SetFlexRamFunction(&flashSSDConfig, EEE_ENABLE, 0x00u, NULL);
      DEV_ASSERT(STATUS_SUCCESS == ret);
    }
#endif /* (FEATURE_FLS_HAS_FLEX_NVM == 1u) & (FEATURE_FLS_HAS_FLEX_RAM == 1u) */
    return ret;
}



/*!
 \brief Application Setup
 */
void AppSetup(eLean_config_t config)
{
  /* Init CAN buffer variables */
  flexcan0_msgbuf.buf_size      = FLEXCAN_BUFFER_SIZE;
  flexcan0_msgbuf.index         = 0;
  flexcan0_msgbuf.free_slot     = FLEXCAN_BUFFER_SIZE;
  flexcan0_msgbuf.buf_full      = false;

  /* Init IC_t struct */
  core.iam20680.status          = IAM20680_UNCONFIGURED;
  core.lis3mdl.status           = LIS3MDL_UNCONFIGURED;

  /* ######################################### */
  /* Install CAN interrupt and enable it */
  INT_SYS_InstallHandler(CAN0_ORed_0_15_MB_IRQn, &CAN0_IRQHandler, NULL);
  INT_SYS_EnableIRQ(CAN0_ORed_0_15_MB_IRQn);

  INT_SYS_InstallHandler(CAN0_ORed_16_31_MB_IRQn, &CAN0_IRQHandler, NULL);
  INT_SYS_EnableIRQ(CAN0_ORed_16_31_MB_IRQn);

  /* Enable global interrupt */
  INT_SYS_EnableIRQGlobal();

  /* ######################################### */
  /* Initialize SystemBasisChip UJA1169 - FlexCAN0 */
  OSIF_TimeDelay(100);
  SBCSetup();

  /* ######################################### */
  /* Init IAM20680 */
  core.iam20680.status = iam20680_Configure(config.iam20680_selftest);

  /* ######################################### */
  /* Init LIS3MDL */

  core.lis3mdl.status = LIS3MDL_SelfTest(config.lis3mdl_selftest);

  if (core.lis3mdl.status != LIS3MDL_NO_ERROR)
  {
    print("LIS3MDL_SelfTest failure\n");
  }
  else
  {
    core.lis3mdl.status = LIS3MDL_Init();

    if (core.lis3mdl.status != LIS3MDL_NO_ERROR)
    {
      print("LIS3MDL_Init failure\n");
    }
  }
}
