/*
 * interrupts.c
 *
 *  Created on: 21 mag 2018
 *      Author: Andrea Costa
 */
#include "eLean.h"



/* TMR0 Interrupt handler */
void ftmTimerMC0ISR (void)
{
  /* Run real-time user code */
  TMR0_step();

  /* Clear FTM Timer Overflow flag */
  FTM_DRV_ClearStatusFlags(INST_FLEXTIMER_MC0, (uint32_t)FTM_TIME_OVER_FLOW_FLAG);
}



/* TMR1 Interrupt handler */
void ftmTimerMC1ISR (void)
{
  /* Run real-time user code */
  TMR1_step();

  /* Clear FTM Timer Overflow flag */
  FTM_DRV_ClearStatusFlags(INST_FLEXTIMER_MC1, (uint32_t)FTM_TIME_OVER_FLOW_FLAG);
}


void CAN0_IRQHandler(void)
{
  FLEXCAN_IRQHandler(0, &flexcan0_msgbuf);
}
