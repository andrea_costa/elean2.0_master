/*
 * FlexCanDrv.c
 *
 *  Created on: 17 mar 2018
 *      Author: Andrea_Costa
 */
#include "eLean.h"


/* Table of base addresses for CAN instances. */
static CAN_Type * const g_flexcanBase[] = CAN_BASE_PTRS;

#define RxFifoFilterTableOffset         0x18U
#define MSG_BUF_SIZE  4    /* Msg Buffer Size. (CAN 2.0AB: 2 hdr +  2 data= 4 words) */

/* Determines the RxFIFO Filter element number */
#define RxFifoFilterElementNum(x) (((x) + 1U) * 8U)

// TODO: da fixare, servono per debug CAN
uint8_t  TxFailure_private;
uint8_t  MBCode_private;


/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_Disable
 * Description   : Disable FlexCAN module.
 * This function will disable FlexCAN module.
 *
 *END**************************************************************************/
void FLEXCAN_Disable(CAN_Type * base)
{
    /* To access the memory mapped registers */
    /* Entre disable mode (hard reset). */
    if(((base->MCR & CAN_MCR_MDIS_MASK) >> CAN_MCR_MDIS_SHIFT) == 0U)
    {
        /* Clock disable (module) */
        base->MCR = (base->MCR & ~CAN_MCR_MDIS_MASK) | CAN_MCR_MDIS(1U);

        /* Wait until disable mode acknowledged */
        while (((base->MCR & CAN_MCR_LPMACK_MASK) >> CAN_MCR_LPMACK_SHIFT) == 0U) {}
    }
}



/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_Enable
 * Description   : Enable FlexCAN module.
 * This function will enable FlexCAN module.
 *
 *END**************************************************************************/
void FLEXCAN_Enable(CAN_Type * base)
{
    /* Check for low power mode */
    if(((base->MCR & CAN_MCR_LPMACK_MASK) >> CAN_MCR_LPMACK_SHIFT) == 1U)
    {
        /* Enable clock */
        base->MCR = (base->MCR & ~CAN_MCR_MDIS_MASK) | CAN_MCR_MDIS(0U);
        base->MCR = (base->MCR & ~CAN_MCR_FRZ_MASK) | CAN_MCR_FRZ(0U);
        base->MCR = (base->MCR & ~CAN_MCR_HALT_MASK) | CAN_MCR_HALT(0U);
        /* Wait until enabled */
        while (((base->MCR & CAN_MCR_LPMACK_MASK) >> CAN_MCR_LPMACK_SHIFT) != 0U) {}
    }
}


/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_EnterFreezeMode
 * Description   : Enter the freeze mode.
 *
 *END**************************************************************************/
void FLEXCAN_EnterFreezeMode(CAN_Type * base)
{
  base->MCR = (base->MCR & ~CAN_MCR_FRZ_MASK) | CAN_MCR_FRZ(1U);
  base->MCR = (base->MCR & ~CAN_MCR_HALT_MASK) | CAN_MCR_HALT(1U);

  /* Wait for entering the freeze mode */
  while (((base->MCR & CAN_MCR_FRZACK_MASK) >> CAN_MCR_FRZACK_SHIFT) == 0U) {}
}


/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_ExitFreezeMode
 * Description   : Exit of freeze mode.
 *
 *END**************************************************************************/
void FLEXCAN_ExitFreezeMode(CAN_Type * base)
{
  base->MCR = (base->MCR & ~CAN_MCR_HALT_MASK) | CAN_MCR_HALT(0U);
  base->MCR = (base->MCR & ~CAN_MCR_FRZ_MASK) | CAN_MCR_FRZ(0U);

  /* Wait till exit freeze mode */
  while (((base->MCR & CAN_MCR_FRZACK_MASK) >> CAN_MCR_FRZACK_SHIFT) != 0U) {}
}






/*FUNCTION**********************************************************************
 *
 * Function Name : FlexCANInitRxFIFO
 * Description   : Initialize CAN interface with RxFIFO enabled
 *
 *END**************************************************************************/
status_t FlexCANInitRxFIFO(uint8_t instance, uint8_t MBused, can_baudrate_t baudrate)
{
  uint8_t i = 0;
  uint32_t CANIndexSelected = 0;

  if (instance == 0)
    CANIndexSelected = PCC_FlexCAN0_INDEX;
  else if (instance == 1)
    CANIndexSelected = PCC_FlexCAN1_INDEX;
  else if (instance == 2)
    CANIndexSelected = PCC_FlexCAN2_INDEX;
  else
    return STATUS_UNSUPPORTED;

  if ((baudrate != BAUDRATE_500K) && (baudrate != BAUDRATE_1000K))
    return STATUS_UNSUPPORTED;


  CAN_Type * base = g_flexcanBase[instance];

  PCC->PCCn[CANIndexSelected] |= PCC_PCCn_CGC_MASK;       /* CGC=1: enable clock to FlexCANx */

  if (CANIndexSelected == PCC_FlexCAN0_INDEX)
  {
#ifdef OLD_CODE_OK
    /* Check for low power mode */
    if(((base->MCR & CAN_MCR_LPMACK_MASK) >> CAN_MCR_LPMACK_SHIFT) == 1U)
    {
        /* Enable clock */
        base->MCR = (base->MCR & ~CAN_MCR_MDIS_MASK) | CAN_MCR_MDIS(0U);
        base->MCR = (base->MCR & ~CAN_MCR_FRZ_MASK) | CAN_MCR_FRZ(0U);
        base->MCR = (base->MCR & ~CAN_MCR_HALT_MASK) | CAN_MCR_HALT(0U);
        /* Wait until enabled */
        while (((base->MCR & CAN_MCR_LPMACK_MASK) >> CAN_MCR_LPMACK_SHIFT) != 0U) {}
    }

    // FLEXCAN_EnterFreezeMode
    base->MCR = (base->MCR & ~CAN_MCR_FRZ_MASK) | CAN_MCR_FRZ(1U);
    base->MCR = (base->MCR & ~CAN_MCR_HALT_MASK) | CAN_MCR_HALT(1U);

    // Wait for entering the freeze mode
    while (((base->MCR & CAN_MCR_FRZACK_MASK) >> CAN_MCR_FRZACK_SHIFT) == 0U) {}
#endif

    /* Check for low power mode */
    if(((base->MCR & CAN_MCR_LPMACK_MASK) >> CAN_MCR_LPMACK_SHIFT) == 1U)
    {
        /* Enable clock */
        base->MCR = (base->MCR & ~CAN_MCR_MDIS_MASK) | CAN_MCR_MDIS(0U);
        base->MCR = (base->MCR & ~CAN_MCR_FRZ_MASK) | CAN_MCR_FRZ(0U);
        base->MCR = (base->MCR & ~CAN_MCR_HALT_MASK) | CAN_MCR_HALT(0U);
        /* Wait until enabled */
        while (((base->MCR & CAN_MCR_LPMACK_MASK) >> CAN_MCR_LPMACK_SHIFT) != 0U) {}
    }

    base->MCR   |= CAN_MCR_MDIS_MASK;                       /* MDIS=1: Disable module before selecting clock  */
    base->CTRL1 &= ~CAN_CTRL1_CLKSRC_MASK;                  /* CLKSRC=0: Clock Source = oscillator (8 MHz)    */
    base->MCR   &= ~CAN_MCR_MDIS_MASK;                      /* MDIS=0; Enable module config. (Sets FRZ, HALT) */

    while (!((base->MCR & CAN_MCR_FRZACK_MASK) >> CAN_MCR_FRZACK_SHIFT)) {}
    /* Good practice: wait for FRZACK=1 on freeze mode entry/exit */
  }
  else
  {

    // FLEXCAN_EnterFreezeMode
    base->MCR = (base->MCR & ~CAN_MCR_FRZ_MASK) | CAN_MCR_FRZ(1U);
    base->MCR = (base->MCR & ~CAN_MCR_HALT_MASK) | CAN_MCR_HALT(1U);

    base->MCR   |= CAN_MCR_MDIS_MASK;                       /* MDIS=1: Disable module before selecting clock  */
    base->CTRL1 &= ~CAN_CTRL1_CLKSRC_MASK;                  /* CLKSRC=0: Clock Source = oscillator (8 MHz)    */
    base->MCR   &= ~CAN_MCR_MDIS_MASK;                      /* MDIS=0; Enable module config. (Sets FRZ, HALT) */

    while (!((base->MCR & CAN_MCR_FRZACK_MASK) >> CAN_MCR_FRZACK_SHIFT)) {}
    /* Good practice: wait for FRZACK=1 on freeze mode entry/exit */
  }

  base->MCR |=  CAN_MCR_RFEN_MASK |     /* RXFIFO enabled */
                CAN_MCR_SRXDIS_MASK |   /* Disable self reception */
                CAN_MCR_IRMQ_MASK |     /* Individual mask register used */
                CAN_MCR_AEN_MASK |      /* AEN=1: Abort enabled */
                CAN_MCR_IDAM(0) |       /* ID acceptance mode; 0..Format A */
                CAN_MCR_MAXMB(MBused);  /* mailboxes used - parametric */

  OSIF_TimeDelay(5);

  /* Set baudrate */
  if (baudrate == BAUDRATE_500K)
  {
    base->CTRL1 = 0x00DB0006;           /* Configure for 500 KHz bit time */
                                        /* Time quanta freq = 16 time quanta x 500 KHz bit time= 8MHz */
                                        /* PRESDIV+1 = Fclksrc/Ftq = 8 MHz/8 MHz = 1 */
                                        /* so PRESDIV = 0 */
                                        /* PSEG2 = Phase_Seg2 - 1 = 4 - 1 = 3 */
                                        /* PSEG1 = PSEG2 = 3 */
                                        /* PROPSEG= Prop_Seg - 1 = 7 - 1 = 6 */
                                        /* RJW: since Phase_Seg2 >=4, RJW+1=4 so RJW=3. */
                                        /* SMP = 1: use 3 bits per CAN sample */
                                        /* LPB = 0: loopback disabled */
                                        /* CLKSRC=0 (unchanged): Fcanclk= Fosc= 8 MHz */
  }
  else if (baudrate == BAUDRATE_1000K)
  {
    /* Configure for 1000 KHz bit time, 16 time quanta, cpi_clk = 8MHz, NBT = 8 */
    base->CTRL1 = CAN_CTRL1_PRESDIV(0) |
                  CAN_CTRL1_CLKSRC (0) |
                  CAN_CTRL1_SMP (0) |
                  CAN_CTRL1_RJW(0) |
                  CAN_CTRL1_PSEG1(0) |
                  CAN_CTRL1_PSEG2(1) |
                  CAN_CTRL1_PROPSEG(3);
  }


  //base->CTRL1 = (base->CTRL1 & ~CAN_CTRL1_LOM_MASK) | CAN_CTRL1_LOM(0U);

  if (CANIndexSelected == PCC_FlexCAN0_INDEX)
  {
    for (i = 32; i < 128; i++)
    {
      /* CANx: clear 24 msg bufs x 4 words/msg buf = 96 words*/
      base->RAMn[i] = 0;      /* Clear msg buf word */
    }
  }
  else
  {
    for (i = 24; i < 64; i++)
    {
      /* Clear msg buf word */
      base->RAMn[i] = 0;
    }
  }

  // Individual Mask register
  // TODO: rendere parametrico - e' in funzione delle MB disponibili?
  // quindi vuol dire CAN0 32 e CAN1/2 16
  //for (i = 0; i < 32; i++)
  for (i = 0; i < MBused; i++)
  {
    /* In FRZ mode, init CANx 32 msg buf filters */
    //base->RXIMR[i] = 0x00000000;    // Don't check IDs for incoming messages
    base->RXIMR[i] = 0xFFFFFFFF;  // Check all ID bits for incoming messages
  }

  /* Global acceptance mask
   * since CAN_MCR_IRMQ_MASK bit is asserted, RXMGMASK has no effect
   */
  base->RXMGMASK = 0x0;

  /* Set the number of the RX FIFO filters needed */
  uint32_t numOfFilters = 0; // FLEXCAN_RX_FIFO_ID_FILTERS_8
  base->CTRL2 = (base->CTRL2 & ~CAN_CTRL2_RFFN_MASK) | ((numOfFilters << CAN_CTRL2_RFFN_SHIFT) & CAN_CTRL2_RFFN_MASK);


  /* Rx FIFO global acceptance mask
   * if Rx FIFO is enabled, RXFGMASK is used to mask the Rx FIFO ID Filter Table
   * that do not have a corresponding RXIMR according to CAN_CTRL2[RFFN] field setting
   */
  //base->RXFGMASK = (CAN_RXFGMASK_FGM_MASK << CAN_ID_EXT_SHIFT) & (CAN_ID_STD_MASK));
  //uint32_t stdMask = CAN_RXFGMASK_FGM_MASK;
  //(base->RXFGMASK) = (((uint32_t)(((uint32_t)(stdMask)) << CAN_ID_STD_SHIFT)) & CAN_ID_STD_MASK);
  //base->RXFGMASK = 0x1FFFFFFF;
  base->RXFGMASK = 0xFFFFFFFF;

  /* Set RxFIFO Filters - Parametric */
  uint16_t ID;
  uint16_t ID_index;
  for (i = 0; i < RxFIFO_FILTER_MAX_NUMBER; i++)
  {
    ID        = CAN_ID_RxFIFO_Filters.Filters[instance].IDs[i];
    ID_index  = CAN_ID_RxFIFO_Filters.Filters[instance].MBs[i];
    base->RAMn[ 6*MSG_BUF_SIZE + ID_index] = ((0)<<30 | ((ID)<<18) << 1);
  }

  /* Remaining MB as RX */
  /* Configure Message Buffer for data reception */
#ifdef ORIGINAL
  base->RAMn[11*MSG_BUF_SIZE + 0] &= (0xFFFFFFFF ^ !CAN_WMBn_CS_IDE_MASK); /* MB[4] will look for a standard ID */
  base->RAMn[11*MSG_BUF_SIZE + 1] &= (0xFFFFFFFF ^ !CAN_WMBn_ID_ID_MASK);  /* Set STD_ID=0 Temporarily */

  base->RAMn[11*MSG_BUF_SIZE + 1] |= 0x444<<18;         /* Receive ID is 0x444 */
  base->RAMn[11*MSG_BUF_SIZE + 0] |= 0x4<<24;           /* CODE=4: RX EMPTY */
#endif

  uint32_t interrupt_mask = 0;
  /* Set MB Rx Filters - Parametric */
  uint16_t MB;

  for (i = 0; i < RX_MAILBOXES_MAX_NUMBER; i++)
  {
    ID        = CAN_ID_MB_Filters.Filters[instance].IDs[i];
    MB  			= CAN_ID_MB_Filters.Filters[instance].MBs[i];

  	if ((MB == MAILBOX_NOT_USED) || (MB > 31)  || (MB < 8))
  		continue;

    base->RAMn[MB*MSG_BUF_SIZE + 0] &= (0xFFFFFFFF ^ !CAN_WMBn_CS_IDE_MASK); /* MB[x] will look for a standard ID */
    base->RAMn[MB*MSG_BUF_SIZE + 1] &= (0xFFFFFFFF ^ !CAN_WMBn_ID_ID_MASK);  /* Set STD_ID=0 Temporarily */

    base->RAMn[MB*MSG_BUF_SIZE + 1] |= ID<<18;         /* Receive ID is 0x444 */
    base->RAMn[MB*MSG_BUF_SIZE + 0] |= 0x4<<24;           /* CODE=4: RX EMPTY */

    interrupt_mask |= (1UL << MB);
  }

  //base->RXMGMASK = 0x1FFFFFFF;                             /* Global acceptance mask */

  /* Interrupt Mask */
  //base->IMASK1 = 0x000000FF;    // First 8 MB

  base->IMASK1 = 0x000000FF | interrupt_mask;

  base->MCR &= ~CAN_MCR_HALT_MASK;        /* Negate HALT bit */

  /*
    ----- RxFIFO -----
    This bit controls whether the Rx FIFO feature is enabled or not. When RFEN is set, MBs 0 to 5 cannot be
    used for normal reception and transmission because the corresponding memory region (0x80-0xDC) is
    used by the FIFO engine as well as additional MBs (up to 32, depending on CAN_CTRL2[RFFN] setting)
    which are used as Rx FIFO ID Filter Table elements.
   */

  /* Good practice: wait for FRZACK to clear (not in freeze mode) */
  while ((base->MCR && CAN_MCR_FRZACK_MASK) >> CAN_MCR_FRZACK_SHIFT) {}

  /* Good practice: wait for NOTRDY to clear (module ready) */
  while ((base->MCR && CAN_MCR_NOTRDY_MASK) >> CAN_MCR_NOTRDY_SHIFT) {}

  return STATUS_SUCCESS;
}


/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_GetMaxMbNum
 * Description   : Computes the maximum RAM size occupied by MBs.
 *
 *END**************************************************************************/
static uint32_t FLEXCAN_GetMaxMbNum(const CAN_Type * base)
{
    uint32_t i, ret = 0;
    CAN_Type * const flexcanBase[] = CAN_BASE_PTRS;
    const uint32_t maxMbNum[] = FEATURE_CAN_MAX_MB_NUM_ARRAY;

    for (i = 0; i < CAN_INSTANCE_COUNT; i++)
    {
        if (base == flexcanBase[i])
        {
            ret = maxMbNum[i];
        }
    }

    return ret;
}

/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_ClearRAM
 * Description   : Clears FlexCAN memory positions that require initialization.
 *
 *END**************************************************************************/
static void FLEXCAN_ClearRAM(CAN_Type * base)
{
    uint32_t databyte;
    uint32_t RAM_size = FLEXCAN_GetMaxMbNum(base) * 4U;
    uint32_t RXIMR_size = FLEXCAN_GetMaxMbNum(base);
    volatile uint32_t *RAM = base->RAMn;

    /* Clear MB region */
    for (databyte = 0; databyte < RAM_size; databyte++) {
        RAM[databyte] = 0x0;
    }

    RAM = base->RXIMR;

    /* Clear RXIMR region */
    for (databyte = 0; databyte < RXIMR_size; databyte++) {
        RAM[databyte] = 0x0;
    }

#if defined(CPU_S32V234)

    /* Set WRMFRZ bit in CTRL2 Register to grant write access to memory */
    base->CTRL2 = (base->CTRL2 & ~CAN_CTRL2_WRMFRZ_MASK) | CAN_CTRL2_WRMFRZ(1U);

    uint32_t ram_addr = (uint32_t)base + (uint32_t)FEATURE_CAN_RAM_OFFSET;
    RAM = (volatile uint32_t *)ram_addr;

    /* Clear RXMGMASK, RXFGMASK, RX14MASK, RX15MASK RAM mapping */
    RAM[FEATURE_CAN_RXMGMASK_RAM_ADDRESS_OFFSET] = 0;
    RAM[FEATURE_CAN_RXFGMASK_RAM_ADDRESS_OFFSET] = 0;
    RAM[FEATURE_CAN_RX14MASK_RAM_ADDRESS_OFFSET] = 0;
    RAM[FEATURE_CAN_RX15MASK_RAM_ADDRESS_OFFSET] = 0;

    /* Clear SMB FD region */
    for (databyte = FEATURE_CAN_SMB_FD_START_ADDRESS_OFFSET; databyte < (uint32_t)FEATURE_CAN_SMB_FD_END_ADDRESS_OFFSET; databyte++) {
            RAM[databyte] = 0;
    }

    /* Clear WRMFRZ bit in CTRL2 Register to restrict write access to memory */
    base->CTRL2 = (base->CTRL2 & ~CAN_CTRL2_WRMFRZ_MASK) | CAN_CTRL2_WRMFRZ(0U);

#endif
}


/*FUNCTION**********************************************************************
 *
 * Function Name : FlexCANInitRxFIFO
 * Description   : Initialize CAN interface with RxFIFO enabled
 *
 *END**************************************************************************/
status_t FlexCANInit(uint8_t instance, uint8_t MBused, can_baudrate_t baudrate)
{
  uint8_t i = 0;
  uint32_t CANIndexSelected = 0;

  if (instance == 0)
    CANIndexSelected = PCC_FlexCAN0_INDEX;
  else if (instance == 1)
    CANIndexSelected = PCC_FlexCAN1_INDEX;
  else if (instance == 2)
    CANIndexSelected = PCC_FlexCAN2_INDEX;
  else
    return STATUS_UNSUPPORTED;

  if ((baudrate != BAUDRATE_500K) && (baudrate != BAUDRATE_1000K))
    return STATUS_UNSUPPORTED;

  CAN_Type * base = g_flexcanBase[instance];


  PCC->PCCn[CANIndexSelected] |= PCC_PCCn_CGC_MASK;       /* CGC=1: enable clock to FlexCANx */

  if (CANIndexSelected == PCC_FlexCAN0_INDEX)
  {
    /* Check for low power mode */
    if(((base->MCR & CAN_MCR_LPMACK_MASK) >> CAN_MCR_LPMACK_SHIFT) == 1U)
    {
        print("+B+\n");
        /* Enable clock */
        base->MCR = (base->MCR & ~CAN_MCR_MDIS_MASK) | CAN_MCR_MDIS(0U);
        base->MCR = (base->MCR & ~CAN_MCR_FRZ_MASK) | CAN_MCR_FRZ(0U);
        base->MCR = (base->MCR & ~CAN_MCR_HALT_MASK) | CAN_MCR_HALT(0U);
        /* Wait until enabled */
        while (((base->MCR & CAN_MCR_LPMACK_MASK) >> CAN_MCR_LPMACK_SHIFT) != 0U) {}
    }

    // FLEXCAN_EnterFreezeMode
    base->MCR = (base->MCR & ~CAN_MCR_FRZ_MASK) | CAN_MCR_FRZ(1U);
    base->MCR = (base->MCR & ~CAN_MCR_HALT_MASK) | CAN_MCR_HALT(1U);

    while (!((base->MCR & CAN_MCR_FRZACK_MASK) >> CAN_MCR_FRZACK_SHIFT)) {}
    /* Good practice: wait for FRZACK=1 on freeze mode entry/exit */

    /* Reset the FLEXCAN */
    base->MCR = (base->MCR & ~CAN_MCR_SOFTRST_MASK) | CAN_MCR_SOFTRST(1U);

    /* Wait for reset cycle to complete */
    while (((base->MCR & CAN_MCR_SOFTRST_MASK) >> CAN_MCR_SOFTRST_SHIFT) != 0U) {}

    FLEXCAN_ClearRAM(base);
  }
  else
  {
    // FLEXCAN_EnterFreezeMode
    base->MCR = (base->MCR & ~CAN_MCR_FRZ_MASK) | CAN_MCR_FRZ(1U);
    base->MCR = (base->MCR & ~CAN_MCR_HALT_MASK) | CAN_MCR_HALT(1U);

    base->MCR   |= CAN_MCR_MDIS_MASK;                       /* MDIS=1: Disable module before selecting clock  */
    base->CTRL1 &= ~CAN_CTRL1_CLKSRC_MASK;                  /* CLKSRC=0: Clock Source = oscillator (8 MHz)    */
    base->MCR   &= ~CAN_MCR_MDIS_MASK;                      /* MDIS=0; Enable module config. (Sets FRZ, HALT) */

    while (!((base->MCR & CAN_MCR_FRZACK_MASK) >> CAN_MCR_FRZACK_SHIFT)) {}
    /* Good practice: wait for FRZACK=1 on freeze mode entry/exit */
  }

  base->MCR |=  CAN_MCR_SRXDIS_MASK |   /* Disable self reception */
                CAN_MCR_IRMQ_MASK |     /* Individual mask register used */
                CAN_MCR_AEN_MASK |      /* AEN=1: Abort enabled */
                CAN_MCR_IDAM(0) |       /* ID acceptance mode; 0..Format A */
                CAN_MCR_MAXMB(MBused);  /* mailboxes used - parametric */

  OSIF_TimeDelay(5);

  /* Set baudrate */
  if (baudrate == BAUDRATE_500K)
  {
    base->CTRL1 = 0x00DB0006;           /* Configure for 500 KHz bit time */
                                        /* Time quanta freq = 16 time quanta x 500 KHz bit time= 8MHz */
                                        /* PRESDIV+1 = Fclksrc/Ftq = 8 MHz/8 MHz = 1 */
                                        /* so PRESDIV = 0 */
                                        /* PSEG2 = Phase_Seg2 - 1 = 4 - 1 = 3 */
                                        /* PSEG1 = PSEG2 = 3 */
                                        /* PROPSEG= Prop_Seg - 1 = 7 - 1 = 6 */
                                        /* RJW: since Phase_Seg2 >=4, RJW+1=4 so RJW=3. */
                                        /* SMP = 1: use 3 bits per CAN sample */
                                        /* LPB = 0: loopback disabled */
                                        /* CLKSRC=0 (unchanged): Fcanclk= Fosc= 8 MHz */
  }
  else if (baudrate == BAUDRATE_1000K)
  {
    /* Configure for 1000 KHz bit time, 16 time quanta, cpi_clk = 8MHz, NBT = 8 */
    base->CTRL1 = CAN_CTRL1_PRESDIV(0) |
                  CAN_CTRL1_CLKSRC (0) |
                  CAN_CTRL1_SMP (0) |
                  CAN_CTRL1_RJW(0) |
                  CAN_CTRL1_PSEG1(0) |
                  CAN_CTRL1_PSEG2(1) |
                  CAN_CTRL1_PROPSEG(3);
  }

  if (CANIndexSelected == PCC_FlexCAN0_INDEX)
  {

    FLEXCAN_ClearRAM(base);
#ifdef ORIG
    for (i = 0; i < 128; i++)
    {
      /* CANx: clear 24 msg bufs x 4 words/msg buf = 96 words*/
      base->RAMn[i] = 0;      /* Clear msg buf word */
    }
#endif
  }
  else
  {
    for (i = 0; i < 64; i++)
    {
      /* Clear msg buf word */
      base->RAMn[i] = 0;
    }
  }

  // Individual Mask register
  // TODO: rendere parametrico - e' in funzione delle MB disponibili?
  // quindi vuol dire CAN0 32 e CAN1/2 16
  //for (i = 0; i < 32; i++)
  for (i = 0; i < MBused; i++)
  {
    /* In FRZ mode, init CANx 32 msg buf filters */
    //base->RXIMR[i] = 0x00000000;    // Don't check IDs for incoming messages
    base->RXIMR[i] = 0xFFFFFFFF;  // Check all ID bits for incoming messages
  }

  /* Global acceptance mask
   * since CAN_MCR_IRMQ_MASK bit is asserted, RXMGMASK has no effect
   */
  base->RXMGMASK = 0x0;

  /* Rx FIFO global acceptance mask
   * if Rx FIFO is enabled, RXFGMASK is used to mask the Rx FIFO ID Filter Table
   * that do not have a corresponding RXIMR according to CAN_CTRL2[RFFN] field setting
   */
  base->RXFGMASK = 0xFFFFFFFF;

  /* Remaining MB as RX */
  /* Configure Message Buffer for data reception */
#ifdef ORIGINAL
  base->RAMn[11*MSG_BUF_SIZE + 0] &= (0xFFFFFFFF ^ !CAN_WMBn_CS_IDE_MASK); /* MB[4] will look for a standard ID */
  base->RAMn[11*MSG_BUF_SIZE + 1] &= (0xFFFFFFFF ^ !CAN_WMBn_ID_ID_MASK);  /* Set STD_ID=0 Temporarily */

  base->RAMn[11*MSG_BUF_SIZE + 1] |= 0x444<<18;         /* Receive ID is 0x444 */
  base->RAMn[11*MSG_BUF_SIZE + 0] |= 0x4<<24;           /* CODE=4: RX EMPTY */
#endif

  uint32_t interrupt_mask = 0;
  /* Set MB Rx Filters - Parametric */
  uint16_t MB;
  uint16_t ID;


  for (i = 0; i < RX_MAILBOXES_MAX_NUMBER; i++)
  {
    ID        = CAN_ID_MB_Filters.Filters[instance].IDs[i];
    MB        = CAN_ID_MB_Filters.Filters[instance].MBs[i];

    if ((MB == MAILBOX_NOT_USED) || (MB > 31))
      continue;

    base->RAMn[MB*MSG_BUF_SIZE + 0] &= (0xFFFFFFFF ^ !CAN_WMBn_CS_IDE_MASK); /* MB[x] will look for a standard ID */
    base->RAMn[MB*MSG_BUF_SIZE + 1] &= (0xFFFFFFFF ^ !CAN_WMBn_ID_ID_MASK);  /* Set STD_ID=0 Temporarily */

    base->RAMn[MB*MSG_BUF_SIZE + 1] |= ID<<18;         /* Receive ID is 0x444 */
    base->RAMn[MB*MSG_BUF_SIZE + 0] |= 0x4<<24;           /* CODE=4: RX EMPTY */

    interrupt_mask |= (1UL << MB);
  }

  //base->RXMGMASK = 0x1FFFFFFF;                             /* Global acceptance mask */

  /* Interrupt Mask */
  base->IMASK1 = interrupt_mask;

  base->MCR &= ~CAN_MCR_HALT_MASK;        /* Negate HALT bit */

  /* Good practice: wait for FRZACK to clear (not in freeze mode) */
  while ((base->MCR && CAN_MCR_FRZACK_MASK) >> CAN_MCR_FRZACK_SHIFT) {}

  /* Good practice: wait for NOTRDY to clear (module ready) */
  while ((base->MCR && CAN_MCR_NOTRDY_MASK) >> CAN_MCR_NOTRDY_SHIFT) {}
}



/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_GetMsgBuffRegion
 * Description   : Returns the start of a MB area, based on its index.
 *
 *END**************************************************************************/
static volatile uint32_t* FLEXCAN_GetMsgBuffRegion(uint8_t instance, uint32_t msgBuffIdx)
{
  CAN_Type * base                 = g_flexcanBase[instance];
  uint8_t payload_size            = 8;
  uint8_t arbitration_field_size  = 8U;
  uint32_t ramBlockSize           = 512U;
  uint32_t ramBlockOffset;

  uint8_t mb_size   = (uint8_t)(payload_size + arbitration_field_size);
  uint8_t maxMbNum  = (uint8_t)(ramBlockSize / mb_size);

  ramBlockOffset = 128U * (msgBuffIdx / (uint32_t)maxMbNum);

  /* Multiply the MB index by the MB size (in words) */
  uint32_t mb_index = ramBlockOffset + ((msgBuffIdx % (uint32_t)maxMbNum) * ((uint32_t)mb_size >> 2U));

  return &(base->RAMn[mb_index]);
}


static inline uint32_t RxFifoOcuppiedLastMsgBuff(uint32_t x)
{
    return (5U + ((((x) + 1U) * 8U) / 4U));
}


/*!
 * @brief Checks if the Flexible Data rate feature is enabled.
 *
 * @param   base    The FlexCAN base address
 * @return  true if enabled; false if disabled
 */
static inline bool FLEXCAN_IsFDEnabled(uint32_t mcr)
{
    //return (((CAN0->MCR & CAN_MCR_FDEN_MASK) >> CAN_MCR_FDEN_SHIFT) != 0U);
  return (((mcr & CAN_MCR_FDEN_MASK) >> CAN_MCR_FDEN_SHIFT) != 0U);
}


/*FUNCTION**********************************************************************
 *
 * Function Name: FLEXCAN_ComputeDLCValue
 * Description  : Computes the DLC field value, given a payload size (in bytes).
 *
 *END**************************************************************************/
static uint8_t FLEXCAN_ComputeDLCValue(
        uint8_t payloadSize)
{
    uint8_t ret;

    if (payloadSize <= 8U)
    {
        ret = payloadSize;
    }
    else if ((payloadSize > 8U) && (payloadSize <= 12U))
    {
        ret = CAN_DLC_VALUE_12_BYTES;
    }
    else if ((payloadSize > 12U) && (payloadSize <= 16U))
    {
        ret = CAN_DLC_VALUE_16_BYTES;
    }
    else if ((payloadSize > 16U) && (payloadSize <= 20U))
    {
        ret = CAN_DLC_VALUE_20_BYTES;
    }
    else if ((payloadSize > 20U) && (payloadSize <= 24U))
    {
        ret = CAN_DLC_VALUE_24_BYTES;
    }
    else if ((payloadSize > 24U) && (payloadSize <= 32U))
    {
        ret = CAN_DLC_VALUE_32_BYTES;
    }
    else if ((payloadSize > 32U) && (payloadSize <= 48U))
    {
        ret = CAN_DLC_VALUE_48_BYTES;
    }
    else if ((payloadSize > 48U) && (payloadSize <= 64U))
    {
        ret = CAN_DLC_VALUE_64_BYTES;
    }
    else
    {
        /* The argument is not a valid payload size */
        ret = 0xFFU;
    }

    return ret;
}


uint32_t FlexCANtransmit(uint8_t instance, uint32_t msgBuffIdx, uint8_t dataLen, uint16_t msgId, uint8_t *msgData)
{
  // TODO: devo pulire l'interrupt relativo al TX del MB selezionato prima di mandare il messaggio?
  // TODO: rendere il clean parametrico + verifica che il MB selezionato sia valido
  // TODO: gestione FLEXCAN_MSG_ID_STD vs FLEXCAN_MSG_ID_EXT

  CAN_Type * base = g_flexcanBase[instance];

  /* Clear CANx MBx flag without clearing the others */
  base->IFLAG1 = 1<<msgBuffIdx;

  status_t stat = STATUS_SUCCESS;

  uint32_t val1, val2 = 1;
  volatile uint32_t *flexcan_mb         = FLEXCAN_GetMsgBuffRegion(instance, msgBuffIdx);
  volatile uint32_t *flexcan_mb_id      = &flexcan_mb[1];
  volatile uint8_t  *flexcan_mb_data    = (volatile uint8_t *)(&flexcan_mb[2]);
  volatile uint32_t *flexcan_mb_data_32 = &flexcan_mb[2];
  uint32_t *msgData_32                  = (uint32_t *)msgData;

  flexcan_msgbuff_code_status_t cs;

  cs.msgIdType      = 0; //FLEXCAN_MSG_ID_STD;
  cs.fd_padding     = 0;
  cs.fd_enable      = false;
  cs.enable_brs     = false;
  cs.dataLen        = dataLen;
  cs.code           = (uint32_t) FLEXCAN_TX_DATA;

  // Check if the MB selected is out of range
  if (msgBuffIdx >= (((base->MCR) & CAN_MCR_MAXMB_MASK) >> CAN_MCR_MAXMB_SHIFT) )
  {
    stat = STATUS_CAN_BUFF_OUT_OF_RANGE;
  }

  /* Check if RX FIFO is enabled*/
  if (((base->MCR & CAN_MCR_RFEN_MASK) >> CAN_MCR_RFEN_SHIFT) != 0U)
  {
      /* Get the number of RX FIFO Filters*/
      val1 = (((base->CTRL2) & CAN_CTRL2_RFFN_MASK) >> CAN_CTRL2_RFFN_SHIFT);
      /* Get the number if MBs occupied by RX FIFO and ID filter table*/
      /* the Rx FIFO occupies the memory space originally reserved for MB0-5*/
      /* Every number of RFFN means 8 number of RX FIFO filters*/
      /* and every 4 number of RX FIFO filters occupied one MB*/
      val2 = RxFifoOcuppiedLastMsgBuff(val1);
      if (msgBuffIdx <= val2)
      {
        stat = STATUS_CAN_BUFF_OUT_OF_RANGE;
      }
  }

  if (stat == STATUS_SUCCESS)
  {
    // Get MB status -> CODE
    uint8_t MB_code = (uint8_t) ((flexcan_mb[0] & CAN_CS_CODE_MASK) >> CAN_CS_CODE_SHIFT);

    // If MB in not INACTIVE
    if ((MB_code != 8) && (MB_code != 0))
    {
      MBCode_private    = MB_code;
      TxFailure_private = true;
      return STATUS_ERROR;
      // TODO: Write ABORT to request an abortion of the transmission
    }


    /* Make sure the BRS bit will not be ignored */
    if (FLEXCAN_IsFDEnabled(base->MCR) && cs.enable_brs)
    {
      base->FDCTRL = (base->FDCTRL & ~CAN_FDCTRL_FDRATE_MASK) | CAN_FDCTRL_FDRATE(1U);
    }

    uint32_t databyte;
    uint32_t flexcan_mb_config = 0;

    /* Compute the value of the DLC field */
    uint8_t dlc_value = FLEXCAN_ComputeDLCValue((uint8_t)cs.dataLen);


    /* Copy user's buffer into the message buffer data area */
    if (msgData != NULL)
    {
        uint8_t payload_size = FLEXCAN_ComputePayloadSize(dlc_value);
        for (databyte = 0; databyte < (cs.dataLen & ~3U); databyte += 4U)
        {
          FlexcanSwapBytesInWord(msgData_32[databyte >> 2U], flexcan_mb_data_32[databyte >> 2U]);
        }
        for ( ; databyte < cs.dataLen; databyte++)
        {
            flexcan_mb_data[FlexcanSwapBytesInWordIndex(databyte)] =  msgData[databyte];
        }
        /* Add padding, if needed */
        for (databyte = cs.dataLen; databyte < payload_size; databyte++)
        {
            flexcan_mb_data[FlexcanSwapBytesInWordIndex(databyte)] = cs.fd_padding;
        }
    }

    /* Clean up the arbitration field area */
    *flexcan_mb     = 0;
    *flexcan_mb_id  = 0;

    /* Set the ID according the format structure */
    if (cs.msgIdType == 1) // FLEXCAN_MSG_ID_EXT
    {
        /* ID [28-0] */
        *flexcan_mb_id &= ~(CAN_ID_STD_MASK | CAN_ID_EXT_MASK);
        *flexcan_mb_id |= (msgId & (CAN_ID_STD_MASK | CAN_ID_EXT_MASK));

        /* Set IDE */
        flexcan_mb_config |= CAN_CS_IDE_MASK;

        /* Clear SRR bit */
        flexcan_mb_config &= ~CAN_CS_SRR_MASK;
    }
    if(cs.msgIdType == 0) // FLEXCAN_MSG_ID_STD
    {
        /* ID[28-18] */
        *flexcan_mb_id &= ~CAN_ID_STD_MASK;
        *flexcan_mb_id |= (msgId << CAN_ID_STD_SHIFT) & CAN_ID_STD_MASK;

        /* make sure IDE and SRR are not set */
        flexcan_mb_config &= ~(CAN_CS_IDE_MASK | CAN_CS_SRR_MASK);
    }

    /* Set the length of data in bytes */
    flexcan_mb_config &= ~CAN_CS_DLC_MASK;
    flexcan_mb_config |= ((uint32_t)dlc_value << CAN_CS_DLC_SHIFT) & CAN_CS_DLC_MASK;


    /* Set MB CODE */
    if (cs.code != (uint32_t)FLEXCAN_TX_NOT_USED)
    {
        if (cs.code == (uint32_t)FLEXCAN_TX_REMOTE)
        {
            /* Set RTR bit */
            flexcan_mb_config |= CAN_CS_RTR_MASK;
        }

        /* Reset the code */
        flexcan_mb_config &= ~CAN_CS_CODE_MASK;

        /* Set the code */
        if (cs.fd_enable)
        {
            flexcan_mb_config |= ((cs.code << CAN_CS_CODE_SHIFT) & CAN_CS_CODE_MASK) | CAN_MB_EDL_MASK;
        }
        else
        {
            flexcan_mb_config |= (cs.code << CAN_CS_CODE_SHIFT) & CAN_CS_CODE_MASK;
        }

        if (cs.enable_brs)
        {
            flexcan_mb_config |= CAN_MB_BRS_MASK;
        }

        // Copy configurations
        *flexcan_mb |= flexcan_mb_config;
    }
  }


#ifdef SEND

  CAN1->RAMn[ msgBuffIdx * MSG_BUF_SIZE + 2] = 0xA5112233; /* MBx word 2: data word 0 */
  CAN1->RAMn[ msgBuffIdx * MSG_BUF_SIZE + 3] = 0x44556677; /* MBx word 3: data word 1 */


  CAN1->RAMn[ msgBuffIdx * MSG_BUF_SIZE + 1] = 0x14440000; /* MBx word 1: Tx msg with STD ID 0x511 */
  CAN1->RAMn[ msgBuffIdx * MSG_BUF_SIZE + 0] = 0x0C400000 | dataLen <<CAN_WMBn_CS_DLC_SHIFT;   /* MBx word 0: */
                                                                                        /* EDL,BRS,ESI=0: CANFD not used */
                                                                                        /* CODE=0xC: Activate msg buf to transmit */
                                                                                        /* IDE = 0: Standard ID */
                                                                                        /* SRR = 1 Tx frame (not req'd for std ID) */
                                                                                        /* RTR = 0: data, not remote tx request frame*/
                                                                                        /* DLC = 8 bytes */

  // TODO: capire se ha senso mettere il wait + clean flag dopo l'invio
  //while (!(CAN1->IFLAG1 & 0x100)) {};  /* Wait for CAN 0 MB 8 flag */
  //CAN1->IFLAG1 = 0x00000100;       /* Clear CAN 0 MB 8 flag without clearing others*/
#endif
  //CAN1->IFLAG1 = 0x00000100;       /* Clear CAN 0 MB 8 flag without clearing others*/

  /* Wait for CANx MBx flag */
  //while (!(CAN1->IFLAG1 & (1<<msgBuffIdx))) {};
  base->IFLAG1 = 1 << msgBuffIdx;

  return stat;
}



/*FUNCTION**********************************************************************
 *
 * Function Name : FLEXCAN_ComputePayloadSize
 * Description   : Computes the maximum payload size (in bytes), given a DLC
 * field value.
 *
 *END**************************************************************************/
uint8_t FLEXCAN_ComputePayloadSize(uint8_t dlcValue)
{
    uint8_t ret;

    if (dlcValue <= 8U)
    {
        ret = dlcValue;
    }
    else
    {
        switch (dlcValue) {
        case CAN_DLC_VALUE_12_BYTES:
            ret = 12U;
            break;
        case CAN_DLC_VALUE_16_BYTES:
            ret = 16U;
            break;
        case CAN_DLC_VALUE_20_BYTES:
            ret = 20U;
            break;
        case CAN_DLC_VALUE_24_BYTES:
            ret = 24U;
            break;
        case CAN_DLC_VALUE_32_BYTES:
            ret = 32U;
            break;
        case CAN_DLC_VALUE_48_BYTES:
            ret = 48U;
            break;
        case CAN_DLC_VALUE_64_BYTES:
            ret = 64U;
            break;
        default:
            /* The argument is not a valid DLC size */
            ret = 0xFFU;
            break;
        }
    }
    return ret;
}




void FLEXCAN_IRQHandler(uint8_t instance, flexcan_msgbuf_t *flexcan_msgbuf)
{
  CAN_Type * base = g_flexcanBase[instance];

  /*
   * Check if BUF5I is triggered
   * When RxFifo is enabled represents "Frame available in Rx FIFO"
   */

  /* Check if RX FIFO is enabled*/
  if (((base->MCR & CAN_MCR_RFEN_MASK) >> CAN_MCR_RFEN_SHIFT) != 0U)
  {
    if ((base->IFLAG1 >> 5) & 1U)
    {
      //IRQmask = base->IFLAG1;

      /* If there is free space in the buffer to save incoming message */
      if (flexcan_msgbuf->free_slot != 0)
      {
        /* CAN data, when RxFIFO is enabled, is set into MB0 */
        volatile uint32_t *flexcan_mb_data_32 = &base->RAMn[2];

        /* Get payload size */
        uint8_t flexcan_mb_dlc_value  = (base->RAMn[0] & 0xF0000u) >> 16;
        uint8_t can_real_payload      = FLEXCAN_ComputePayloadSize(flexcan_mb_dlc_value);

        flexcan_msgbuf->flexcan_msg[flexcan_msgbuf->index].dataLen  = can_real_payload;
        flexcan_msgbuf->flexcan_msg[flexcan_msgbuf->index].cs       = base->RAMn[0];

        if ((flexcan_msgbuf->flexcan_msg[flexcan_msgbuf->index].cs & CAN_CS_IDE_MASK) != 0U)
        {
          flexcan_msgbuf->flexcan_msg[flexcan_msgbuf->index].msgId = base->RAMn[1];
        }
        else
        {
          flexcan_msgbuf->flexcan_msg[flexcan_msgbuf->index].msgId = (base->RAMn[1]) >> CAN_ID_STD_SHIFT;
        }

        uint32_t databyte;
        uint32_t mbWord;

        uint32_t *msgData_32 = (uint32_t *)(flexcan_msgbuf->flexcan_msg[flexcan_msgbuf->index].data);

        /* Copy MB[0] data field into user's buffer */
        for (databyte = 0; databyte < can_real_payload; databyte += 4U)
        {
            mbWord = flexcan_mb_data_32[databyte >> 2U];
            FlexcanSwapBytesInWord(mbWord, msgData_32[databyte >> 2U]);
        }

        flexcan_msgbuf->index++;
        flexcan_msgbuf->free_slot--;

        if (flexcan_msgbuf->index >= FLEXCAN_BUFFER_SIZE)
          flexcan_msgbuf->index = 0;
      }
      else
      {
        // TODO: gestire sistuazione buffer CAN pieno
        flexcan_msgbuf->buf_full = true;
      }

      // TODO: non fare il clear di tutti gli interrupt ma solo di quello triggerato
      //base->IFLAG1    = 0x00FF;
      base->IFLAG1 = 1<<5;
    }


    /* Bypass first 8 bit (used by RxFIFO) */
    uint32_t 	mask;
    uint8_t 	flag = 0; // [0-1]
    if ((base->IFLAG1 >> 8) >= 1)
    {
      flexcan_msg_t flexcan_msg;

      uint32_t mb_idx = 8;

      mask = base->IMASK1 & CAN_IMASK1_BUF31TO0M_MASK;
      flag = (uint8_t)(((base->IFLAG1 & mask) >> (mb_idx % 32U)) & 1U);

      while ((flag & 1U) == 0U)
      {
        mb_idx++;
        flag = (uint8_t)(((base->IFLAG1 & mask) >> (mb_idx % 32U)) & 1U);

        if (mb_idx > FEATURE_CAN_MAX_MB_NUM)
        {
            break;
        }
      }

      if (flag != 0)
      {
        volatile uint32_t *flexcan_mb_data_32 = &base->RAMn[mb_idx * MSG_BUF_SIZE + 2];

        /* Get payload size */
        uint8_t flexcan_mb_dlc_value  = (base->RAMn[mb_idx*MSG_BUF_SIZE] & 0xF0000u) >> 16;
        uint8_t can_real_payload      = FLEXCAN_ComputePayloadSize(flexcan_mb_dlc_value);

        flexcan_msg.dataLen  = can_real_payload;
        flexcan_msg.cs       = base->RAMn[mb_idx * MSG_BUF_SIZE];

        if ((flexcan_msg.cs & CAN_CS_IDE_MASK) != 0U)
        {
          flexcan_msg.msgId = base->RAMn[mb_idx * MSG_BUF_SIZE + 1];
        }
        else
        {
          flexcan_msg.msgId = (base->RAMn[mb_idx * MSG_BUF_SIZE + 1]) >> CAN_ID_STD_SHIFT;
        }

        uint32_t databyte;
        uint32_t mbWord;

        uint32_t *msgData_32 = (uint32_t *)(&flexcan_msg.data);

        /* Copy MB data field into user's buffer */
        for (databyte = 0; databyte < can_real_payload; databyte += 4U)
        {
            mbWord = flexcan_mb_data_32[databyte >> 2U];
            FlexcanSwapBytesInWord(mbWord, msgData_32[databyte >> 2U]);
        }

        if (instance == 0)
        {
          CAN0_MsgHandler(&flexcan_msg);
        }
        else if (instance == 1)
        {
          /* Nothing to do here.. */
        }
        else if (instance == 2)
        {
          CAN2_MsgHandler(&flexcan_msg);
        }

        base->IFLAG1 = 1 << mb_idx;
      }
    }
  }
  // If RxFIFO is NOT enabled
  else
  {
    flexcan_msg_t flexcan_msg;

    uint32_t  mb_idx = 0;
    uint32_t  mask;
    uint8_t   flag = 0; // [0-1]

    mask = base->IMASK1 & CAN_IMASK1_BUF31TO0M_MASK;
    flag = (uint8_t)(((base->IFLAG1 & mask) >> (mb_idx % 32U)) & 1U);

    while ((flag & 1U) == 0U)
    {
      mb_idx++;
      flag = (uint8_t)(((base->IFLAG1 & mask) >> (mb_idx % 32U)) & 1U);

      if (mb_idx > FEATURE_CAN_MAX_MB_NUM)
      {
          break;
      }
    }

    if (flag != 0)
    {
      volatile uint32_t *flexcan_mb_data_32 = &base->RAMn[mb_idx * MSG_BUF_SIZE + 2];

      /* Get payload size */
      uint8_t flexcan_mb_dlc_value  = (base->RAMn[mb_idx*MSG_BUF_SIZE] & 0xF0000u) >> 16;
      uint8_t can_real_payload      = FLEXCAN_ComputePayloadSize(flexcan_mb_dlc_value);

      flexcan_msg.dataLen  = can_real_payload;
      flexcan_msg.cs       = base->RAMn[mb_idx * MSG_BUF_SIZE];

      if ((flexcan_msg.cs & CAN_CS_IDE_MASK) != 0U)
      {
        flexcan_msg.msgId = base->RAMn[mb_idx * MSG_BUF_SIZE + 1];
      }
      else
      {
        flexcan_msg.msgId = (base->RAMn[mb_idx * MSG_BUF_SIZE + 1]) >> CAN_ID_STD_SHIFT;
      }

      uint32_t databyte;
      uint32_t mbWord;

      uint32_t *msgData_32 = (uint32_t *)(&flexcan_msg.data);

      /* Copy MB data field into user's buffer */
      for (databyte = 0; databyte < can_real_payload; databyte += 4U)
      {
          mbWord = flexcan_mb_data_32[databyte >> 2U];
          FlexcanSwapBytesInWord(mbWord, msgData_32[databyte >> 2U]);
      }

      if (instance == 0)
      {
        CAN0_MsgHandler(&flexcan_msg);
      }
      else if (instance == 1)
      {
        /* Nothing to do here.. */
      }
      else if (instance == 2)
      {
        CAN2_MsgHandler(&flexcan_msg);
      }

      base->IFLAG1 = 1 << mb_idx;
    }
  }
}


/*!
 \brief Get CAN msg from CAN buffer
 */
uint8_t CAN_ReadOneFramefromBufferQueue(flexcan_msgbuf_t *RxBuffer, flexcan_msg_t *RxFrame)
{
  /* If there is a mismatch between the read and the save index, or if the buffer is full */
  if ((RxBuffer->frame_read_cnt == RxBuffer->index) && (RxBuffer->free_slot != 0))
    return false;

  *RxFrame = RxBuffer->flexcan_msg[RxBuffer->frame_read_cnt];
  RxBuffer->frame_read_cnt++;

  if(RxBuffer->frame_read_cnt >= FLEXCAN_BUFFER_SIZE)
  {
    RxBuffer->frame_read_cnt = 0;
  }

  RxBuffer->free_slot++;

  return true;
}


