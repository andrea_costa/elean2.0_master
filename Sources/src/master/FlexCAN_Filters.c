/*
 * FlecCAN_Filters.c
 *
 *  Created on: 03 lug 2018
 *      Author: Andrea_Costa
 */

#include "eLean.h"





/* Define filters for Rx MB (CAN0/CAN1/CAN2)
 */
CAN_ID_MB_Filters_t CAN_ID_MB_Filters =
{
    /* CAN 0 */
    .Filters[0] = {
        /* ID list */
        .IDs = {
            Rx_CAN_HUB_L_DATA,
            Rx_CAN_HUB_L_POS_CURR,
            Rx_CAN_HUB_R_DATA,
            Rx_CAN_HUB_R_POS_CURR,
            Rx_CAN_LIDAR_FRONT_DETECTIONS,
            Rx_CAN_LIDAR_FRONT_DISTANCE,
            Rx_CAN_LIDAR_LEFT_DETECTIONS,
            Rx_CAN_LIDAR_LEFT_DISTANCE,
            Rx_CAN_LIDAR_REAR_DETECTIONS,
            Rx_CAN_LIDAR_REAR_DISTANCE,
            Rx_CAN_LIDAR_RIGHT_DETECTIONS,
            Rx_CAN_LIDAR_RIGHT_DISTANCE,
            Rx_CAN_NAV_U_REF,
            Rx_CAN_RESET,
            0,
            0
        },
        /* MailBox list */
        .MBs = {
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            MAILBOX_NOT_USED,
            MAILBOX_NOT_USED
        }
    },
    /* CAN 1 - MB available from 8 to 15 */
    .Filters[1] = {
        /* ID list */
        .IDs = {0},
        /* MailBox list */
        .MBs = {0}
    },
    /* CAN 2 - MB available from 8 to 15 */
    .Filters[2] = {
        /* ID list */
        .IDs = {
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0
        },
        /* MailBox list */
        .MBs = {
            MAILBOX_NOT_USED,
            MAILBOX_NOT_USED,
            MAILBOX_NOT_USED,
            MAILBOX_NOT_USED,
            MAILBOX_NOT_USED,
            MAILBOX_NOT_USED,
            MAILBOX_NOT_USED,
            MAILBOX_NOT_USED
        }
    }
};




/* Define filters for RxFIFO (CAN0/CAN1/CAN2)
 * !!! Maximum 8 IDs !!!
 */
CAN_ID_RxFIFO_Filters_t CAN_ID_RxFIFO_Filters =
{
      /* CAN 0 */
      .Filters[0] = {
          /* ID list */
          .IDs = {
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0
          },
          /* MailBox list */
          .MBs = {
              0,
              1,
              2,
              3,
              4,
              5,
              6,
              7
          }
      },
      /* CAN 1 */
      .Filters[1] = {
          /* ID list */
          .IDs = {0},
          /* MailBox list */
          .MBs = {0}
      },
      /* CAN 2 */
      .Filters[2] = {
          /* ID list */
          .IDs = {
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0
          },
          /* MailBox list */
          .MBs = {
              0,
              1,
              2,
              3,
              4,
              5,
              6,
              7
          }
      }
};
