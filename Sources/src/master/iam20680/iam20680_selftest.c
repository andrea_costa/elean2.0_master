/*
 * iam20680_selftest.c
 *
 *  Created on: 24 apr 2018
 *      Author: Andrea_Costa
 */


#include "eLean.h"
#include <math.h>




static const uint16_t sSelfTestEquation[256] =
{
  2620, 2646, 2672, 2699, 2726, 2753, 2781, 2808,
  2837, 2865, 2894, 2923, 2952, 2981, 3011, 3041,
  3072, 3102, 3133, 3165, 3196, 3228, 3261, 3293,
  3326, 3359, 3393, 3427, 3461, 3496, 3531, 3566,
  3602, 3638, 3674, 3711, 3748, 3786, 3823, 3862,
  3900, 3939, 3979, 4019, 4059, 4099, 4140, 4182,
  4224, 4266, 4308, 4352, 4395, 4439, 4483, 4528,
  4574, 4619, 4665, 4712, 4759, 4807, 4855, 4903,
  4953, 5002, 5052, 5103, 5154, 5205, 5257, 5310,
  5363, 5417, 5471, 5525, 5581, 5636, 5693, 5750,
  5807, 5865, 5924, 5983, 6043, 6104, 6165, 6226,
  6289, 6351, 6415, 6479, 6544, 6609, 6675, 6742,
  6810, 6878, 6946, 7016, 7086, 7157, 7229, 7301,
  7374, 7448, 7522, 7597, 7673, 7750, 7828, 7906,
  7985, 8065, 8145, 8227, 8309, 8392, 8476, 8561,
  8647, 8733, 8820, 8909, 8998, 9088, 9178, 9270,
  9363, 9457, 9551, 9647, 9743, 9841, 9939, 10038,
  10139, 10240, 10343, 10446, 10550, 10656, 10763, 10870,
  10979, 11089, 11200, 11312, 11425, 11539, 11654, 11771,
  11889, 12008, 12128, 12249, 12371, 12495, 12620, 12746,
  12874, 13002, 13132, 13264, 13396, 13530, 13666, 13802,
  13940, 14080, 14221, 14363, 14506, 14652, 14798, 14946,
  15096, 15247, 15399, 15553, 15709, 15866, 16024, 16184,
  16346, 16510, 16675, 16842, 17010, 17180, 17352, 17526,
  17701, 17878, 18057, 18237, 18420, 18604, 18790, 18978,
  19167, 19359, 19553, 19748, 19946, 20145, 20347, 20550,
  20756, 20963, 21173, 21385, 21598, 21814, 22033, 22253,
  22475, 22700, 22927, 23156, 23388, 23622, 23858, 24097,
  24338, 24581, 24827, 25075, 25326, 25579, 25835, 26093,
  26354, 26618, 26884, 27153, 27424, 27699, 27976, 28255,
  28538, 28823, 29112, 29403, 29697, 29994, 30294, 30597,
  30903, 31212, 31524, 31839, 32157, 32479, 32804
};


/*!
 \brief iam20680 disable self test
 */
void disable_self_test(uint8_t reg_address)
{
  uint8_t resp[2] = {0};
  uint8_t cmd[2]  = {0};

  switch(reg_address)
  {
    case IAM20680_GYRO_CONFIG:
      cmd[0] = IAM20680_WRITE_REGISTER_BITMASK | IAM20680_GYRO_CONFIG;
      cmd[1] = iam20680_mems.gyro_config.GYRO_CONFIG_register & IAM20680_DISABLE_ST;
      LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 2, 1000);
      break;
    case IAM20680_ACCEL_CONFIG:
      cmd[0] = IAM20680_WRITE_REGISTER_BITMASK | IAM20680_ACCEL_CONFIG;
      cmd[1] = iam20680_mems.accel_config.ACCEL_CONFIG_register & IAM20680_DISABLE_ST;
      LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 2, 1000);
      break;
  }
}


/*!
 \brief iam20680 enable self test on given axis
 */
void enable_self_test_on_axis(unsigned int reg_address, unsigned int self_test_axis)
{
  uint8_t resp[2] = {0};
  uint8_t cmd[2] = {0};

  switch(reg_address)
  {
    case IAM20680_GYRO_CONFIG:
      cmd[0] = IAM20680_WRITE_REGISTER_BITMASK | IAM20680_GYRO_CONFIG;
      cmd[1] = iam20680_mems.gyro_config.GYRO_CONFIG_register | self_test_axis;
      LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 2, 1000);
      break;
    case IAM20680_ACCEL_CONFIG:
      cmd[0] = IAM20680_WRITE_REGISTER_BITMASK | IAM20680_ACCEL_CONFIG;
      cmd[1] = iam20680_mems.accel_config.ACCEL_CONFIG_register | self_test_axis;
      LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 2, 1000);
      break;
  }
}


/*!
 \brief iam20680 read manufacturing values
 */
void read_self_test_manufacturing_values(unsigned int * values)
{
  uint8_t cmd[2]        = {IAM20680_READ_REGISTER_BITMASK | IAM20680_SELF_TEST_X_GYRO, 0};
  uint8_t resp[2]       = {0, 0};
  uint8_t temp_regs[6]  = {0};

  /* ISSUES WITH REGISTER ADDRESS AUTO-INCREMENTING   */
  /* READ ONE REG AT THE TIME                         */

  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 2, 1000);
  temp_regs[0] = resp[1];

  cmd[0] = IAM20680_READ_REGISTER_BITMASK | IAM20680_SELF_TEST_Y_GYRO;
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 2, 1000);
  temp_regs[1] = resp[1];

  cmd[0] = IAM20680_READ_REGISTER_BITMASK | IAM20680_SELF_TEST_Z_GYRO;
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 2, 1000);
  temp_regs[2] = resp[1];

  cmd[0] = IAM20680_READ_REGISTER_BITMASK | IAM20680_SELF_TEST_X_ACCEL;
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 2, 1000);
  temp_regs[3] = resp[1];

  cmd[0] = IAM20680_READ_REGISTER_BITMASK | IAM20680_SELF_TEST_Y_ACCEL;
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 2, 1000);
  temp_regs[4] = resp[1];

  cmd[0] = IAM20680_READ_REGISTER_BITMASK | IAM20680_SELF_TEST_Z_ACCEL;
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 2, 1000);
  temp_regs[5] = resp[1];

  unsigned int gyro_divider = pow(2, iam20680_mems.gyro_config.GYRO_CONFIG_bits.fs_sel);
  unsigned int accel_divider = pow(2, iam20680_mems.accel_config.ACCEL_CONFIG_bits.accel_fs_sel);

  uint8_t i;

  for (i = 0; i < 6; i++)
  {
    if (temp_regs[i] != 0)
    {
      if (i < 3)
        values[i] = (sSelfTestEquation[temp_regs[i] - 1] / gyro_divider);
      else
        values[i] = (sSelfTestEquation[temp_regs[i] - 1] / accel_divider);
    }
    else
      values[i] = 0;
  }
  return;
}


/*!
 \brief iam20680 calculate self test magic value
 */
double iam20680_calculate_selftest_magic_value(long int MeanValue, long int STMeanValue, unsigned int self_test_maufacturing_value)
{
  int self_test_response = STMeanValue - MeanValue;
  return (double) self_test_response / (double)self_test_maufacturing_value;
}


/*!
 \brief iam20680 gyro self test
 */
void enable_self_test_gyro()
{
  uint8_t resp[2] = {0};
  uint8_t cmd[2]  = {0};

  cmd[0] = IAM20680_WRITE_REGISTER_BITMASK | IAM20680_GYRO_CONFIG;
  cmd[1] = 0b11100000 + iam20680_mems.gyro_config.GYRO_CONFIG_bits.fchoice_b;

  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 2, 1000);
}


/*!
 \brief iam20680 acc self test
 */
void enable_self_test_accel()
{
  uint8_t resp[2] = {0};
  uint8_t cmd[2]  = {0};

  cmd[0] = IAM20680_WRITE_REGISTER_BITMASK | IAM20680_ACCEL_CONFIG;
  cmd[1] = 0b11100000 + (iam20680_mems.accel_config.ACCEL_CONFIG_bits.accel_fs_sel << 3);
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 2, 1000);
}


/*!
 \brief iam20680 self test procedure
 */
uint8_t iam20680_self_test_procedure(volatile uint8_t * results)
{
  /* Leggo i valori OTP di self test valutati in fase di produzione del MEMS */
  unsigned int self_test_maufacturing_values[6] = {0};
  read_self_test_manufacturing_values(self_test_maufacturing_values);

  /* contiene differenza tra self test e normal functioning.
   * se il test non viene fatto per un certo asse il valore sar� -1
   */
  uint8_t ret = true; // false se almeno un test su un asse non � passato

  long int sum_x    = 0;
  long int sum_y    = 0;
  long int sum_z    = 0;
  long int sumST_x  = 0;
  long int sumST_y  = 0;
  long int sumST_z  = 0;
  uint16_t i;

  uint8_t cmd[7]  = {IAM20680_READ_REGISTER_BITMASK | IAM20680_GYRO_XOUT_H, 0, 0, 0, 0, 0, 0};
  uint8_t resp[7] = {0};

  /* ################### */
  /* GYROSCOPE SELF_TEST */
  /* ################### */

  /* Read IAM20680_NUM_VAL_FOR_MEAN samples for each gyro axis */
  for(i = 0; i < IAM20680_NUM_VAL_FOR_MEAN; i++)
  {
    LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 7, 2000);
    sum_x += (int16_t)((resp[1] << 8) | resp[2]);
    sum_y += (int16_t)((resp[3] << 8) | resp[4]);
    sum_z += (int16_t)((resp[5] << 8) | resp[6]);
    OSIF_TimeDelay(1);
  }

  sum_x = sum_x / IAM20680_NUM_VAL_FOR_MEAN;
  sum_y = sum_y / IAM20680_NUM_VAL_FOR_MEAN;
  sum_z = sum_z / IAM20680_NUM_VAL_FOR_MEAN;

#ifdef IAM20680_SELFTEST_UART_DEBUG
  sprintf(TXbuf, "GYRO NO ST  %d %d %d\n", sum_x, sum_y, sum_z);
  print(TXbuf);
#endif

  /* Enable self test bit for the gyro axis  */
  enable_self_test_gyro();

  OSIF_TimeDelay(30);

  /* Read IAM20680_NUM_VAL_FOR_MEAN samples for each gyro axis */
  for(i = 0; i < IAM20680_NUM_VAL_FOR_MEAN; i++)
  {
    LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 7, 2000);
    sumST_x += (int16_t)((resp[1] << 8) | resp[2]);
    sumST_y += (int16_t)((resp[3] << 8) | resp[4]);
    sumST_z += (int16_t)((resp[5] << 8) | resp[6]);
    OSIF_TimeDelay(1);
  }

  disable_self_test(IAM20680_GYRO_CONFIG);
  OSIF_TimeDelay(10);

  sumST_x = sumST_x / IAM20680_NUM_VAL_FOR_MEAN;
  sumST_y = sumST_y / IAM20680_NUM_VAL_FOR_MEAN;
  sumST_z = sumST_z / IAM20680_NUM_VAL_FOR_MEAN;

#ifdef IAM20680_SELFTEST_UART_DEBUG
  sprintf(TXbuf, "GYRO ST %d %d %d\n",  sumST_x, sumST_y, sumST_z);
  print(TXbuf);
#endif

  double ST_Value_Gx = iam20680_calculate_selftest_magic_value(sum_x, sumST_x, self_test_maufacturing_values[0]);
  double ST_Value_Gy = iam20680_calculate_selftest_magic_value(sum_y, sumST_y, self_test_maufacturing_values[1]);
  double ST_Value_Gz = iam20680_calculate_selftest_magic_value(sum_z, sumST_z, self_test_maufacturing_values[2]);

  /*  ###################### */
  /* ACCELEROMETER SELF_TEST */
  /*  ###################### */
  cmd[0] = IAM20680_READ_REGISTER_BITMASK | IAM20680_ACCEL_XOUT_H;

  sum_x   = 0;
  sum_y   = 0;
  sum_z   = 0;
  sumST_x = 0;
  sumST_y = 0;
  sumST_z = 0;

  /* Read IAM20680_NUM_VAL_FOR_MEAN samples for each acc axis */
  for(i = 0; i < IAM20680_NUM_VAL_FOR_MEAN; i++)
  {
    LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 7, 2000);
    sum_x += (int16_t)((resp[1] << 8) | resp[2]);
    sum_y += (int16_t)((resp[3] << 8) | resp[4]);
    sum_z += (int16_t)((resp[5] << 8) | resp[6]);
    OSIF_TimeDelay(1);
  }

  sum_x = sum_x / IAM20680_NUM_VAL_FOR_MEAN;
  sum_y = sum_y / IAM20680_NUM_VAL_FOR_MEAN;
  sum_z = sum_z / IAM20680_NUM_VAL_FOR_MEAN;

#ifdef IAM20680_SELFTEST_UART_DEBUG
    sprintf(TXbuf, "ACC NO ST %d %d %d\n", sum_x, sum_y, sum_z);
    print(TXbuf);
#endif

  /* Enable self test bit for the gyro axis  */
  enable_self_test_accel();

  OSIF_TimeDelay(30);

  /* Read IAM20680_NUM_VAL_FOR_MEAN samples for each acc axis */
  for(i = 0; i < IAM20680_NUM_VAL_FOR_MEAN; i++)
  {
    LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 7, 1000);
    sumST_x += ((resp[1] << 8) | resp[2]);
    sumST_y += ((resp[3] << 8) | resp[4]);
    sumST_z += ((resp[5] << 8) | resp[6]);
    OSIF_TimeDelay(1);
  }

  disable_self_test(IAM20680_ACCEL_CONFIG);
  OSIF_TimeDelay(10);

  sumST_x = sumST_x / IAM20680_NUM_VAL_FOR_MEAN;
  sumST_y = sumST_y / IAM20680_NUM_VAL_FOR_MEAN;
  sumST_z = sumST_z / IAM20680_NUM_VAL_FOR_MEAN;

#ifdef IAM20680_SELFTEST_UART_DEBUG
  sprintf(TXbuf, "ACC ST %d %d %d\n", sumST_x, sumST_y, sumST_z);
  print(TXbuf);
#endif

  double ST_Value_Ax = iam20680_calculate_selftest_magic_value(sum_x, sumST_x, self_test_maufacturing_values[3]);
  double ST_Value_Ay = iam20680_calculate_selftest_magic_value(sum_y, sumST_y, self_test_maufacturing_values[4]);
  double ST_Value_Az = iam20680_calculate_selftest_magic_value(sum_z, sumST_z, self_test_maufacturing_values[5]);


#ifdef IAM20680_SELFTEST_UART_DEBUG
  sprintf(TXbuf, "\n* GYRO X - ST Value: %f\n", ST_Value_Gx);
  print(TXbuf);

  sprintf(TXbuf, "\n* GYRO Y - ST Value: %f\n", ST_Value_Gy);
  print(TXbuf);

  sprintf(TXbuf, "\n* GYRO Z - ST Value: %f\n", ST_Value_Gz);
  print(TXbuf);

  sprintf(TXbuf, "\n* ACC X - ST Value: %f\n", ST_Value_Ax);
  print(TXbuf);

  sprintf(TXbuf, "\n* ACC Y - ST Value: %f\n", ST_Value_Ay);
  print(TXbuf);

  sprintf(TXbuf, "\n* ACC Z - ST Value: %f\n", ST_Value_Az);
  print(TXbuf);
#endif

  if (ST_Value_Gx <= 0.5)
    ret = false;
  else
    results[0] = 1;

  if (ST_Value_Gy <= 0.5)
    ret = false;
  else
    results[1] = 1;

  if (ST_Value_Gz <= 0.5)
    ret = false;
  else
    results[2] = 1;

  if ((ST_Value_Ax <= 0.5) && (ST_Value_Ax >= 1.5))
    ret = false;
  else
    results[3] = 1;

  if ((ST_Value_Ay <= 0.5) && (ST_Value_Ay >= 1.5))
    ret = false;
  else
    results[4] = 1;

  if ((ST_Value_Az <= 0.5) && (ST_Value_Az >= 1.5))
    ret = false;
  else
    results[5] = 1;

  return ret;
}

