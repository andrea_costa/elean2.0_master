#include "eLean.h"

struct iam20680_mems_t iam20680_mems;


/*!
 \brief iam20680 - Configure device
 */
uint8_t iam20680_Configure (uint8_t self_test)
{
  iam20680_data_t data;

  OSIF_TimeDelay(15);

  iam20680_device_id (&data);

  if (IAM20680_ID_VAL != (data.device_id & 0x00FF))
      return IAM20680_READ_ID_ERROR;

  /* If self-test is enabled */
  if (self_test == true)
  {
    if (iam20680_self_test_procedure(core.iam20680.STResults) == false)
      return IAM20680_SELFTEST_ERROR;
  }

  OSIF_TimeDelay(15);
  iam20680_initialize_config_variables();

  OSIF_TimeDelay(15);
  iam20680_reset(IAM20680_RESET_VAL);


  OSIF_TimeDelay(15);
  iam20680_set_low_noise_mode(iam20680_mems.pwr_mgmt_1,
                              iam20680_mems.pwr_mgmt_2);

  OSIF_TimeDelay(15);
  iam20680_set_SPI_I2C_mode(iam20680_mems.user_ctrl.USER_CTRL_bits.i2c_if_dis);

  OSIF_TimeDelay(15);
  iam20680_set_sample_rate(iam20680_mems.smplrt_div);

  OSIF_TimeDelay(15);
  iam20680_set_gyro_bw_range(iam20680_mems.config.CONFIG_bits.dlpf_cfg,
                             iam20680_mems.gyro_config.GYRO_CONFIG_bits.fs_sel,
                             iam20680_mems.gyro_config.GYRO_CONFIG_bits.fchoice_b);

  OSIF_TimeDelay(15);
  iam20680_set_acc_bw_range(iam20680_mems.accel_config.ACCEL_CONFIG_bits.accel_fs_sel,
                            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.accel_fchoice_b,
                            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.a_dlpf_cfg);

  return IAM20680_NO_ERROR;
}


/*!
 \brief iam20680 - Get device id
 */
void iam20680_device_id(iam20680_data_t *data)
{
  uint8_t cmd[2] = {IAM20680_READ_REGISTER_BITMASK | IAM20680_WHO_AM_I, 0x00};
  uint8_t resp[2];

  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 2, 1000);

  data->device_id = resp[1];
}


/*!
 \brief iam20680 - Set sample rate
 */
void iam20680_set_sample_rate(uint8_t divider)
{
  uint8_t cmd[2]  = {IAM20680_WRITE_REGISTER_BITMASK | IAM20680_SMPLRT_DIV, divider};
  uint8_t resp[2];

  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 2, 1000);

  /* modifico i valori nella struttura iam20680_mems */
  iam20680_mems.smplrt_div = divider;
}


/*!
 \brief iam20680 - Set low noise mode
 */
void iam20680_set_low_noise_mode(union PWR_MGMT_1_register_t pwr_mgmt_1, union PWR_MGMT_2_register_t pwr_mgmt_2)
{
  uint8_t regs[2]     = {0};
  uint8_t cmd107[2]   = {IAM20680_WRITE_REGISTER_BITMASK | IAM20680_PWR_MGMT_1, pwr_mgmt_1.PWR_MGMT_1_register};
  uint8_t cmd108[2]   = {IAM20680_WRITE_REGISTER_BITMASK | IAM20680_PWR_MGMT_2, pwr_mgmt_2.PWR_MGMT_2_register};


  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd107, &regs, 2, 1000);
  OSIF_TimeDelay(35);
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd108, &regs, 2, 1000);
  OSIF_TimeDelay(35);

  /* Modifico i valori nella struttura iam20680_mems */
  iam20680_mems.pwr_mgmt_1.PWR_MGMT_1_register = pwr_mgmt_1.PWR_MGMT_1_register;
  iam20680_mems.pwr_mgmt_2.PWR_MGMT_2_register = pwr_mgmt_2.PWR_MGMT_2_register;
}


/*!
 \brief iam20680 - Get configurations
 */
void iam20680_get_config(uint8_t * out)
{
  uint8_t cmd28[7]    = {IAM20680_READ_REGISTER_BITMASK | 25, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  uint8_t resp[7]     = {0, 0, 0, 0, 0, 0, 0};

  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd28, &resp, 7, 1000);

  out[0] = resp[1];
  out[1] = resp[2];
  out[2] = resp[3];
  out[3] = resp[4];
  out[4] = resp[5];
  out[5] = resp[6];
}


/*!
 \brief iam20680 - Set accelerometer bandwidth
 */
void iam20680_set_acc_bw_range(uint8_t accel_fs_sel, uint8_t accel_f_choice_b, uint8_t accel_dlpf_cfg)
{
  uint8_t regs[2]     = {0, 0};
  uint8_t cmd28[2]    = {IAM20680_WRITE_REGISTER_BITMASK | IAM20680_ACCEL_CONFIG, accel_fs_sel << 3, 0x00};
  uint8_t cmd29[2]    = {IAM20680_WRITE_REGISTER_BITMASK | IAM20680_ACCEL_CONFIG_2, accel_f_choice_b << 3 | accel_dlpf_cfg};

  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd28, &regs, 2, 1000);
  OSIF_TimeDelay(100);
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd29, &regs, 2, 1000);

  /* modifico i valori nella struttura iam20680_mems */
  iam20680_mems.accel_config.ACCEL_CONFIG_bits.accel_fs_sel           = accel_fs_sel;
  iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.accel_fchoice_b    = accel_f_choice_b;
  iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.accel_fchoice_b    = accel_dlpf_cfg;
}


/*!
 \brief iam20680 - Set gyroscope bandwidth
 */
void iam20680_set_gyro_bw_range (uint8_t gyro_dlpf_cfg, uint8_t gyro_fs_sel, uint8_t gyro_f_choice_b)
{
  uint8_t regs[2]     = {0, 0};
  uint8_t cmd26[2]    = {IAM20680_WRITE_REGISTER_BITMASK | IAM20680_CONFIG, gyro_dlpf_cfg, 0x00};
  uint8_t cmd27[2]    = {IAM20680_WRITE_REGISTER_BITMASK | IAM20680_GYRO_CONFIG, gyro_fs_sel | gyro_f_choice_b};


  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd26, &regs, 2, 1000);
  OSIF_TimeDelay(100);
  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd27, &regs, 2, 1000);

  /* modifico i valori nella struttura iam20680_mems */
  iam20680_mems.config.CONFIG_bits.dlpf_cfg             = gyro_dlpf_cfg;
  iam20680_mems.gyro_config.GYRO_CONFIG_bits.fs_sel     = gyro_fs_sel;
  iam20680_mems.gyro_config.GYRO_CONFIG_bits.fchoice_b  = gyro_f_choice_b;
}


/*!
 \brief iam20680 - Read accelerometer axis
 */
void iam20680_ReadAcc (iam20680_data_t *data)
{
  uint8_t cmd[7]      = {IAM20680_READ_REGISTER_BITMASK | IAM20680_ACCEL_XOUT_H, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  uint8_t resp[7]     = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 7, 1000);

  data->ax = (resp[2]) | (resp[1] << 8);
  data->ay = (resp[4]) | (resp[3] << 8);
  data->az = (resp[6]) | (resp[5] << 8);
}


/*!
 \brief iam20680 - Read gyroscope axis
 */
void iam20680_ReadGyro (iam20680_data_t *data)
{
  uint8_t cmd[7]      = {IAM20680_READ_REGISTER_BITMASK | IAM20680_GYRO_XOUT_H, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  uint8_t resp[7]     = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 7, 1000);

  data->gx = (resp[2]) | (resp[1] << 8);
  data->gy = (resp[4]) | (resp[3] << 8);
  data->gz = (resp[6]) | (resp[5] << 8);
}

/*
void iam20680_ReadIMU(int *Acc, gyro_t *gyro, int *temperature)
{
    unsigned int cmd[] = {IAM20680_READ_REGISTER_BITMASK | ACCEL_XOUT_H};
  unsigned int resp[14] = {0};

  SPI_WriteAndRead(IAM20680_INTERFACE, cmd, 1, resp, 14);

  int Acc_x = (resp[1]) | (resp[0] << 8);
  int Acc_y = (resp[3]) | (resp[2] << 8);
  int Acc_z = (resp[5]) | (resp[4] << 8);

  Acc[0] = Acc_x;
  Acc[1] = Acc_y;
  Acc[2] = Acc_z;

    *temperature = resp[7] | (resp[6] << 8);

    int gyro_x = (resp[9]) | (resp[8] << 8);
  int gyro_y = (resp[11]) | (resp[10] << 8);
  int gyro_z = (resp[13]) | (resp[12] << 8);

  gyro->gx = gyro_x;
    gyro->gy = gyro_y;
    gyro->gz = gyro_z;
}
*/


/*!
 \brief iam20680 - Read accelerometer and gyroscope axis
 */
void iam20680_ReadIMU(iam20680_data_t *data)
{
    uint8_t cmd[15]     = {IAM20680_READ_REGISTER_BITMASK | IAM20680_ACCEL_XOUT_H};
  uint8_t resp[15]      = {0};

  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 15, 1000);

    data->ax            = (resp[2])  | (resp[1] << 8);
    data->ay            = (resp[4])  | (resp[3] << 8);
    data->az            = (resp[6])  | (resp[5] << 8);
    data->temperature   = (resp[8])  | (resp[7] << 8);
    data->gx            = (resp[10]) | (resp[9] << 8);
    data->gy            = (resp[12]) | (resp[11] << 8);
    data->gz            = (resp[14]) | (resp[13] << 8);
}


/*!
 \brief iam20680 - Read temperature
 */
void iam20680_ReadTemperature(int *temperature)
{
  uint8_t cmd[3]      = {IAM20680_READ_REGISTER_BITMASK | IAM20680_TEMP_OUT_H, 0x00, 0x00};
  uint8_t resp[3]     = {0x00, 0x00, 0x00};

  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 3, 1000);

  *temperature = resp[2] | (resp[1] << 8);
}


/*!
 \brief iam20680 - Reset device
 */
void iam20680_reset(uint8_t reset)
{
  uint8_t cmd[2]        = {IAM20680_WRITE_REGISTER_BITMASK | IAM20680_PWR_MGMT_1, reset};
  uint8_t resp[2]       = {0, 0};

  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 2, 1000);
}


/*!
 \brief iam20680 - set mode SPI/I2C
 */
void iam20680_set_SPI_I2C_mode(uint8_t i2c_disabled)
{
  uint8_t resp[2]       = {0x00, 0x00};
  uint8_t cmd[2]        = {IAM20680_WRITE_REGISTER_BITMASK | IAM20680_USER_CTRL, i2c_disabled};

  LPSPI_DRV_MasterTransferBlocking(LPSPICOM2, &cmd, &resp, 2, 1000);
}
/* END OF CONFIGURATION FUNCTIONS */
















