/*
 * iam20680_configurations.c
 *
 *  Created on: 24 apr 2018
 *      Author: Claudio Scandella
 */

#include "iam20680.h"
#include "iam20680_configuration.h"

void iam20680_initialize_config_variables()
{
    switch(IAM20680_GYRO_CONFIGURATION)
    {
        case 0:
            iam20680_mems.config.CONFIG_bits.dlpf_cfg = 0;
            iam20680_mems.gyro_config.GYRO_CONFIG_bits.fchoice_b = 1;
            break;
        case 1:
            iam20680_mems.config.CONFIG_bits.dlpf_cfg = 0;
            iam20680_mems.gyro_config.GYRO_CONFIG_bits.fchoice_b = 2;
            break;
        case 2:
            iam20680_mems.config.CONFIG_bits.dlpf_cfg = 0;
            iam20680_mems.gyro_config.GYRO_CONFIG_bits.fchoice_b = 0;
            break;
        case 3:
            iam20680_mems.config.CONFIG_bits.dlpf_cfg = 1;
            iam20680_mems.gyro_config.GYRO_CONFIG_bits.fchoice_b = 0;
            break;
        case 4:
            iam20680_mems.config.CONFIG_bits.dlpf_cfg = 2;
            iam20680_mems.gyro_config.GYRO_CONFIG_bits.fchoice_b = 0;
            break;
        case 5:
            iam20680_mems.config.CONFIG_bits.dlpf_cfg = 3;
            iam20680_mems.gyro_config.GYRO_CONFIG_bits.fchoice_b = 0;
            break;
        case 6:
            iam20680_mems.config.CONFIG_bits.dlpf_cfg = 4;
            iam20680_mems.gyro_config.GYRO_CONFIG_bits.fchoice_b = 0;
            break;
        case 7:
            iam20680_mems.config.CONFIG_bits.dlpf_cfg = 5;
            iam20680_mems.gyro_config.GYRO_CONFIG_bits.fchoice_b = 0;
            break;
        case 8:
            iam20680_mems.config.CONFIG_bits.dlpf_cfg = 6;
            iam20680_mems.gyro_config.GYRO_CONFIG_bits.fchoice_b = 0;
            break;
        case 9:
            iam20680_mems.config.CONFIG_bits.dlpf_cfg = 7;
            iam20680_mems.gyro_config.GYRO_CONFIG_bits.fchoice_b = 0;
            break;
        default:
            iam20680_mems.config.CONFIG_bits.dlpf_cfg = 5;
            iam20680_mems.gyro_config.GYRO_CONFIG_bits.fchoice_b = 0;
    }

    switch(IAM20680_ACCEL_CONFIGURATION)
    {
        case 0:
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.a_dlpf_cfg = 0;
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.accel_fchoice_b = 1;
            break;
        case 1:
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.a_dlpf_cfg = 1;
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.accel_fchoice_b = 0;
            break;
        case 2:
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.a_dlpf_cfg = 2;
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.accel_fchoice_b = 0;
            break;
        case 3:
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.a_dlpf_cfg = 3;
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.accel_fchoice_b = 0;
            break;
        case 4:
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.a_dlpf_cfg = 4;
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.accel_fchoice_b = 0;
            break;
        case 5:
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.a_dlpf_cfg = 5;
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.accel_fchoice_b = 0;
            break;
        case 6:
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.a_dlpf_cfg = 6;
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.accel_fchoice_b = 0;
            break;
        case 7:
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.a_dlpf_cfg = 7;
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.accel_fchoice_b = 0;
            break;
        default:
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.a_dlpf_cfg = 5;
            iam20680_mems.accel_config_2.ACCEL_CONFIG_2_bits.accel_fchoice_b = 0;
    }

    switch(IAM20680_ACCEL_RANGE)
    {
        case 2:
            iam20680_mems.accel_config.ACCEL_CONFIG_bits.accel_fs_sel = 0;
            break;
        case 4:
            iam20680_mems.accel_config.ACCEL_CONFIG_bits.accel_fs_sel = 1;
            break;
        case 8:
            iam20680_mems.accel_config.ACCEL_CONFIG_bits.accel_fs_sel = 2;
            break;
        case 16:
            iam20680_mems.accel_config.ACCEL_CONFIG_bits.accel_fs_sel = 3;
            break;
        default:
            iam20680_mems.accel_config.ACCEL_CONFIG_bits.accel_fs_sel = 3;
    }

    switch(IAM20680_GYRO_RANGE)
    {
        case 250:
            iam20680_mems.gyro_config.GYRO_CONFIG_bits.fs_sel = 0;
            break;
        case 500:
            iam20680_mems.gyro_config.GYRO_CONFIG_bits.fs_sel = 1;
            break;
        case 1000:
            iam20680_mems.gyro_config.GYRO_CONFIG_bits.fs_sel = 2;
            break;
        case 2000:
            iam20680_mems.gyro_config.GYRO_CONFIG_bits.fs_sel = 3;
            break;
        default:
            iam20680_mems.gyro_config.GYRO_CONFIG_bits.fs_sel = 0;
    }

    iam20680_mems.user_ctrl.USER_CTRL_bits.i2c_if_dis = 1;
#ifdef IAM20680_I2C_MODE
    iam20680_mems.user_ctrl.USER_CTRL_bits.i2c_if_dis = 0;
#endif

    iam20680_mems.pwr_mgmt_1.PWR_MGMT_1_bits.temp_dis = 0;
#ifdef TEMP_SENS_OFF
    iam20680_mems.pwr_mgmt_1.PWR_MGMT_1_bits.temp_dis = 1;
#endif

    iam20680_mems.smplrt_div = (1000 / IAM20680_SAMPLE_RATE) - 1;
//    iam20680_mems.rate_divider = (1000 / IAM20680_SAMPLE_RATE) - 1;

    iam20680_mems.pwr_mgmt_2.PWR_MGMT_2_bits.stby_xa = STBY_XA;
    iam20680_mems.pwr_mgmt_2.PWR_MGMT_2_bits.stby_ya = STBY_YA;
    iam20680_mems.pwr_mgmt_2.PWR_MGMT_2_bits.stby_za = STBY_ZA;
    iam20680_mems.pwr_mgmt_2.PWR_MGMT_2_bits.stby_xg = STBY_XG;
    iam20680_mems.pwr_mgmt_2.PWR_MGMT_2_bits.stby_yg = STBY_YG;
    iam20680_mems.pwr_mgmt_2.PWR_MGMT_2_bits.stby_zg = STBY_ZG;

    iam20680_mems.pwr_mgmt_1.PWR_MGMT_1_bits.clksel = 1;;
}
